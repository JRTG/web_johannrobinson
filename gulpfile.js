var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify-es').default,
  uglifycss = require('gulp-uglifycss'),
  sourcemaps = require('gulp-sourcemaps'),
  connect = require('gulp-connect-php7'),
  browserSync = require("browser-sync"),
  stylus = require('gulp-stylus'),
  obfs = require('gulp-javascript-obfuscator'),
  fs = require('fs'),
  pkg = JSON.parse(fs.readFileSync('package.json')),
  nib = require('nib'),
  replace = require('gulp-replace');

const paths = {
  php: './wp-content/themes/' + pkg.name + '/**/*.php',
  js: {
    prodDest: './wp-content/themes/' + pkg.name + '/assets/js/',
    workerDest: './',
    devComp: [
      './wp-content/themes/' + pkg.name + '/assets/js/app.utils/**/*.js',
      './wp-content/themes/' + pkg.name + '/assets/js/app.services/**/*.js',
      './wp-content/themes/' + pkg.name + '/assets/js/app.components/**/*.js',
      './wp-content/themes/' + pkg.name + '/assets/js/app.modules/**/*.js',
      './wp-content/themes/' + pkg.name + '/assets/js/app.pages/**/*.js'
    ],
    workerComp: './wp-content/themes/' + pkg.name + '/assets/js/app.workers/*.js'
  },
  styl: {
    devComp: [
      './wp-content/themes/' + pkg.name + '/assets/stylus/styles.styl',
      './wp-content/themes/' + pkg.name + '/assets/stylus/styles-small.styl',
      './wp-content/themes/' + pkg.name + '/assets/stylus/styles-admin.styl'
    ],
    devWatch: './wp-content/themes/' + pkg.name + '/assets/stylus/**/*.styl',
    prodDest: './wp-content/themes/' + pkg.name + '/assets/css/'
  }
};


var js = function (done) {
  // [DEV JS navegador]
  console.log('[JS] init compilation');
  gulp.src(paths.js.devComp)
    .pipe(replace('html`', '`'))
    .pipe(sourcemaps.init())
    .pipe(concat('app.main.js'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest(paths.js.prodDest));
  done();
};

var jswk = function (done) {
  // [DEV JS Service Worker]
  console.log('[JS] worker compilation');
  gulp.src(paths.js.workerComp)
    .pipe(sourcemaps.init())
    .pipe(concat('app.worker.js'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest(paths.js.workerDest));
  done();
};

var styl = function () {
  // [DEV STYL compilation]
  console.log('[STYL] init compilation');
  return gulp.src(paths.styl.devComp)
    .pipe(stylus({ use: nib(), import: ['nib'] }))
    .pipe(gulp.dest(paths.styl.prodDest));
};

var pjs = function (done) {
  // [PROD JS navegador]
  console.log('[JS Prod] init compilation');
  gulp.src(paths.js.devComp)
    .pipe(replace('html`', '`'))
    .pipe(concat('app.main.js'))
    .pipe(uglify({ safari10: true }))
    .pipe(obfs({
      compact: true
    }))
    .pipe(gulp.dest(paths.js.prodDest));
  done();
};

var pjswk = function (done) {
  // [PROD JS Service Worker]
  console.log('[JS Prod] worker compilation');
  gulp.src(paths.js.workerComp)
    .pipe(concat('app.worker.js'))
    .pipe(uglify({ safari10: true }))
    .pipe(obfs({
      compact: true
    }))
    .pipe(gulp.dest(paths.js.workerDest));
  done();
};

var pstyl = function () {
  // place code for your default task here
  console.log('[STYL] init compilation');
  return gulp.src(paths.styl.devComp)
    .pipe(stylus({ use: nib(), import: ['nib'] }))
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest(paths.styl.prodDest));
};

var browserSyncReload = function (done) {
  browserSync.reload();
  done();
}

var connection = function (done) {
  connect.server({ base: './', hostname: 'localhost', port: '8080' }, function () {
    browserSync({
      proxy: 'localhost',
      port: 8080,
      ui: false
    });
  });
  done();
};

var watch = function (done) {
  console.log('[W]');
  gulp.watch(paths.styl.devWatch, gulp.series(styl, browserSyncReload));
  gulp.watch(paths.js.devComp, gulp.series(js, browserSyncReload));
  gulp.watch(paths.js.workerComp, gulp.series(jswk, browserSyncReload));
  gulp.watch(paths.php, browserSyncReload);
  done();
};



var prod = gulp.series(pstyl, pjs, pjswk);
var def = gulp.parallel(styl, js, jswk, connection, watch);

gulp.task('prod', prod);

gulp.task('default', def);
