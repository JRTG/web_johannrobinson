<?php

use JRAPP\Pages\PoliticasPage;

get_header();
$pagePoliticas = (new PoliticasPage())->datos;
// var_dump($PAGE_POLITICAS);
// print("<pre>".print_r($PAGE_POLITICAS,true)."</pre>");
?>
<div id="page" class="politicas">
  <div id="politicas">
    <c-banner-page
      :image="{src: '<?= $pagePoliticas["bannerPrincipal"]['imagen']['url'] ?>', alt: '<?= $pagePoliticas["bannerPrincipal"]['imagen']['alt'] ?>'}"
      fondo="<?= $pagePoliticas["bannerPrincipal"]["fondo"]['url'] ?>"
      :title='<?= json_encode(explode('|', $pagePoliticas["bannerPrincipal"]["texto"])) ?>'
      title-tag="h1"
      title-mod-text="u-tx-ov-el u-ws-nw"
      :title-controls="[{
          todo: 'share',
          icon: 'icon-share',
          mod:'primary',
          theme: 'circle',
          title: 'compartir'
        }]"
    ></c-banner-page>
    <div class="c-breadcrumd">
      <div class="c-breadcrumd__inner g-area-segura">
        <div class="c-breadcrumd__routelist">
          <a href="/" rel="home" class="c-breadcrumd__routelist__item g-ajax-page"><span>Inicio</span></a>
          <a href="javascript:void(0)" class="c-breadcrumd__routelist__item"><span>Políticas</span></a>
        </div>
        <!-- <div class="c-breadcrumd__date"></div> -->
      </div>
    </div>
    <section class="g-section">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <div class="g-section__texto">
            <?= $pagePoliticas['seccPoliticas']['terminos']; ?>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
