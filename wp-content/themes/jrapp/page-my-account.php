<?php

use JRAPP\Posttypes\IdeaType;

get_header();
$ideaType = new IdeaType();
$listaIdeas = $ideaType->listarIdeas(['oder_by'=>'post_date','order'=>'DESC','meta_query'=>[['key'=>'user','value'=>get_current_user_id(),'compare'=>'=']]],false)['listaIdeas'];

// var_dump($PAGE_POLITICAS);
// print("<pre>".print_r($PAGE_POLITICAS,true)."</pre>");
?>
<div id="page" class="my-account">
  <!-- <pre style="font-size: 10px;"><?= print_r([], true) ?></pre> -->
  <div id="my-account">
    <c-banner-page
      fondo="#f7b90a"
      :title='["Bienvenido", currentUser.display_name]'
      title-tag="h1"
      title-mod-text="u-tx-ov-el u-ws-nw"
      :title-controls="[{
          todo: 'share',
          icon: 'icon-share',
          mod:'primary',
          theme: 'circle',
          title: 'compartir'
        },
        {
          todo: 'external',
          url: 'https://www.youtube.com/watch?v=j1A3VreBm4A',
          target: '_blank',
          icon: 'icon-reply',
          mod:'secondary',
          theme: 'circle',
          title: 'go'
        }]"
    ></c-banner-page>
    <section class="g-section">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <c-title
            :title="['Mis Ideas', 'Más Recientes']"
            size="small"
          ></c-title>
          <div class="g-section__texto g-section__texto--middle">
            <?= $pageNosotros['seccionProyectos']['texto'] ?>
          </div>
        </div>
        <div class="g-area-segura">
          <!-- <pre style="font-size: 10px;"><?= print_r(json_encode($listaIdeas), true); ?></pre> -->
          <div class="c-card-idea" v-for='(item, index) in uJsonParse(`<?= json_encode($listaIdeas) ?>`)'>
            <h3>{{item.title}}</h3>
            <p>{{item.descripcion}}</p>
            <span>Perfil de usuario: {{item.perfil.post_title}}</span>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
