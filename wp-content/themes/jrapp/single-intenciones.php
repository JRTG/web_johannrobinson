<?php

use JRAPP\Posttypes\IntencionType;
use JRAPP\Util\Apariencia;
get_header();
$Intencion = (new IntencionType())->obtenerIntencion();
// print('<pre style="font-size:10px;">'.print_r($Contacto->datos,true).'</pre>');
?>

<div id="page" class="intencion">
  <div id="intencion">
    <c-banner-page
      fondo="<?= $Intencion['banner']['url']; ?>"
      title-tag="h1"
      title-size="medium"
      :title-icon="{src: '<?= $Intencion['imagen']['url']; ?>', alt: '<?= $Intencion['imagen']['alt']; ?>'}"
      :title='<?= json_encode(Apariencia::formatearTexto($Intencion['title'])); ?>'
      :title-controls="[{
          theme:'circle',
          mod:'primary',
          icon:'icon-share',
          todo:'share',
          title:'compartir'
        }]"
    ></c-banner-page>
    <div class="c-breadcrumd">
        <div class="c-breadcrumd__inner g-area-segura">
            <div class="c-breadcrumd__routelist">
                <a href="/" rel="home" class="c-breadcrumd__routelist__item g-ajax-page"><span>Inicio</span></a>
                <a href="/intenciones" rel="intenciones" class="c-breadcrumd__routelist__item"><span>Intenciones</span></a>
                <a href="javascript:void(0)" class="c-breadcrumd__routelist__item"><span><?= $Intencion['title'] ?></span></a>
            </div>
            <!-- <div class="c-breadcrumd__date"></div> -->
        </div>
    </div>
    <section class="intencion-contenido">
      <?php for ($i = 0; $i < count($Intencion['contenido']); $i++) {
          $ic = $Intencion['contenido'][$i];
      ?>
      <div class="g-section g-section--theme-content <?=$i%2==0?'':'is-alternate'?>">
          <div class="g-section__inner has-small-spaces g-area-segura u-flex u-flex-fd-<?=$i%2==0?'r':'rr'?> u-flex-fw-w u-flex-ai-ce u-flex-fd-xs-c">
              <div class="g-section__texto g-section__texto--middle">
                  <?= $ic['contenido_texto'] ?>
              </div>
              <div class="g-section__image g-section__image--middle">
                  <img class="lozad" data-src="<?= $ic['contenido_imagen']['url'] ?>" alt="<?= $ic['contenido_imagen']['alt'] ?>">
              </div>
          </div>
      </div>
      <?php } ?>
    </section>
  </div>
</div>
<?php get_footer(); ?>
