<?php
use JRAPP\Util\Apariencia;
use JRAPP\Posttypes\PostType;
get_header();
$Post = (new PostType())->obtenerPost();
$listaPosts = (new PostType())->listarPosts(['orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => 8], false)['listaPosts'];
// print('<pre style="font-size:10px;">'.print_r($Contacto->datos,true).'</pre>');
?>

<div id="page" class="post">
  <div id="post">
    <c-banner-page
      fondo="<?= $Post['banner']['url']; ?>"
      title-tag="h1"
      title-size="medium"
      title-pre="Blog | <?= $Post['categoria'][0]->name ?>"
      :title='<?= json_encode(Apariencia::formatearTexto($Post['title'])); ?>'
      :title-controls="[{
          theme:'circle',
          mod:'primary',
          icon:'icon-share',
          todo:'share',
          title:'compartir'
        }]"
    ></c-banner-page>
    <div class="c-breadcrumd">
      <div class="c-breadcrumd__inner g-area-segura">
          <div class="c-breadcrumd__routelist">
              <a href="/" rel="home" class="c-breadcrumd__routelist__item g-ajax-page"><span>Inicio</span></a>
              <a href="/blog" rel="blog" class="c-breadcrumd__routelist__item"><span>Blog</span></a>
              <a href="javascript:void(0)" class="c-breadcrumd__routelist__item"><span><?= $Post['title'] ?></span></a>
          </div>
          <div class="c-breadcrumd__date">
              Publicado el <?= $Post['date']; ?>
          </div>
      </div>
    </div>
    <section class="post-contenido">
      <?php for ($i = 0; $i < count($Post['contenido']); $i++) {
          $pc = $Post['contenido'][$i];
      ?>
      <div class="g-section g-section--theme-content <?=$i%2==0?'':'is-alternate'?>">
          <div class="g-section__inner has-small-spaces g-area-segura u-flex u-flex-fd-<?=$i%2==0?'r':'rr'?> u-flex-fw-w u-flex-ai-ce u-flex-fd-xs-c">
              <div class="g-section__texto g-section__texto--middle">
                  <?= $pc['contenido_texto'] ?>
              </div>
              <div class="g-section__image g-section__image--middle">
                  <img class="lozad" data-src="<?= $pc['contenido_imagen']['url'] ?>" alt="<?= $pc['contenido_imagen']['alt'] ?>">
              </div>
          </div>
      </div>
      <?php } ?>
    </section>
    <section class="post-blog g-section has-background-color">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <div class="g-title g-title--small">
              <h2 class="g-title__text">
                  <div class="g-title__text__t1">
                      No te pierdas nuestras
                  </div>
                  <div class="g-title__text__t2">
                      Curiosidades Digitales
                  </div>
              </h2>
              <!-- <div class="g-title__controls">
            </div> -->
          </div>
        </div>
        <div class="g-area-segura g-area-segura--full-mobile">
          <c-slider ref="sliderPost" theme="theme-02" :conf="sliderConf.sliderT2">
            <c-slide
              v-for='(itemPost, index) in uJsonParse(`<?= json_encode($listaPosts) ?>`)'
              :key="index">
              <c-post-card :data="itemPost"></c-post-card>
            </c-slide>
          </c-slider>
        </div>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
