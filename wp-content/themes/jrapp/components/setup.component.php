<c-popup
  icon="<?= MEDIA; ?>/app/03.png"
  :title="['Queremos darte una','Mejor experiencia']"
  v-on:close="this.closeAll"
  :show="this.activePopup == 'setup'">
  <div class="g-section__texto u-tx-al-ce">
    <h3>Si quieres tenernos en tu celular o pc</h3>
    <p>Instala la Aplicación, pues te ayudará a ahorrar datos de tu plan y visualizar contenido guardado sin conexión a internet.</p>
  </div>
  <a id="dt-setup" href="" class="g-boton">
    <em>Instalar</em>
  </a>
</c-popup>
