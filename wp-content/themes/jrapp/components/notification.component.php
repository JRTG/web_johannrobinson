<c-popup icon="<?= MEDIA; ?>/app/04.png" :title="['Siempre al día','¡Suscríbete!']" v-on:close="this.closeAll" :show="this.activePopup == 'notification'">
  <div class="g-section__texto u-tx-al-ce">
    <h3>¡Vamos Suscríbete!</h3>
    <p>Al suscribirte te llegarán notificaciones sobre las actividades, programas, promociones y descuentos.</p>
  </div>
  <c-form-suscriber ref="formSuscriberPopup"></c-form-suscriber>
</c-popup>
