<c-popup
  icon="<?= MEDIA; ?>/app/03.png"
  :title="['Tenemos cosas geniales','Inicia Sesión']"
  v-on:close="this.closeAll"
  :show="this.activePopup == 'sesion'">
  <c-form-sesion ref="formSuscriberPopup"></c-form-sesion>
</c-popup>
