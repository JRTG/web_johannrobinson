<?php

use JRAPP\Pages\NosotrosPage;
use JRAPP\Posttypes\ProyectoType;
use JRAPP\Util\Apariencia;

get_header();
$pageNosotros = (new NosotrosPage())->datos;
$proyectoType    = new ProyectoType();
$listaProyectos = $proyectoType->listarProyectos(['orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => 8], false)['listaProyectos'];
// print('<pre style="font-size:10px;">'.print_r($Contacto->datos,true).'</pre>');
?>

<div id="page" class="nosotros">
    <!-- <pre style="font-size:10px;"><?= print_r([], true) ?></pre> -->
  <div id="nosotros">
    <c-banner-page
      :image="{src: '<?= $pageNosotros["bannerPrincipal"]['imagen']['url'] ?>', alt: '<?= $pageNosotros["bannerPrincipal"]['imagen']['alt'] ?>'}"
      fondo="<?= $pageNosotros["bannerPrincipal"]["fondo"]['url'] ?>"
      :title='<?= json_encode(explode('|', $pageNosotros["bannerPrincipal"]["texto"])) ?>'
      title-tag="h1"
      title-mod-text="u-tx-ov-el u-ws-nw"
      :title-controls="[{
          todo: 'share',
          icon: 'icon-share',
          mod:'primary',
          theme: 'circle',
          title: 'compartir'
        }]"
    ></c-banner-page>
    <div class="c-breadcrumd">
        <div class="c-breadcrumd__inner g-area-segura">
            <div class="c-breadcrumd__routelist">
                <a href="/" rel="home" class="c-breadcrumd__routelist__item g-ajax-page"><span>Inicio</span></a>
                <a href="javascript:void(0)" class="c-breadcrumd__routelist__item"><span>Nosotros</span></a>
            </div>
            <!-- <div class="c-breadcrumd__date"></div> -->
        </div>
    </div>
    <c-slider mod="nosotros-historia-slider" :conf="sliderConf.sliderHistoria">
      <c-slide
        mod="hs-item-t0"
        v-for='(slide, index) in uJsonParse(`<?= json_encode($pageNosotros['seccionHistoria']['hitos']) ?>`)'
        :key="index">
        <div class="nhs-item">
          <div class="nhs-top">
            <div class="nhs-inner g-area-segura">
              <div class="nhs-image icon-jr">
                <img class="lozad" :data-src="slide['hito_imagen'].url" :alt="slide['hito_imagen'].alt" height="260" width="260">
              </div>
            </div>
          </div>
          <div class="nhs-bottom">
            <div class="nhs-inner g-area-segura">
              <div class="g-section__texto u-tx-al-ce" v-html="slide['hito_texto']">

              </div>
            </div>
          </div>
        </div>
      </c-slide>
    </c-slider>
    <section class="nosotros-procesos g-section">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <c-title
            :title='<?= json_encode(Apariencia::formatearTexto($pageNosotros['seccionProceso']['titulo'])) ?>'
            size="small"
          ></c-title>
          <div class="g-section__texto g-section__texto--middle">
            <?= $pageNosotros['seccionProceso']['texto'] ?>
          </div>
          <div class="u-flex u-flex-fw-w u-flex-jc-ce">
            <?php
            for ($i = 0; $i < count($pageNosotros['seccionProceso']['procesos']); $i++) {
                $proc = $pageNosotros['seccionProceso']['procesos'][$i];
            ?>
            <div class="c-proceso">
              <span class="c-proceso__number"><?= ($i + 1) ?></span>
              <div class="c-proceso__image">
                <img class="lozad" data-src="<?= $proc['proc_imagen']['url'] ?>" alt="<?= $proc['proc_imagen']['alt'] ?>">
              </div>
              <h4 class="c-proceso__nombre"><?= $proc['proc_nombre'] ?></h4>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </section>
    <section class="nosotros-proyectos g-section has-background-color">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <c-title
            :title='<?= json_encode(Apariencia::formatearTexto($pageNosotros['seccionProyectos']['titulo'])) ?>'
            size="small"
          ></c-title>
          <div class="g-section__texto g-section__texto--middle">
            <?= $pageNosotros['seccionProyectos']['texto'] ?>
          </div>
        </div>
        <div class="g-area-segura g-area-segura--full-mobile">
          <c-slider theme="theme-02" :conf="sliderConf.sliderT2">
            <c-slide
              v-for='(itemProyecto, index) in uJsonParse(`<?= json_encode($listaProyectos) ?>`)'
              :key="index">
              <c-proyecto-card :data="itemProyecto"></c-proyecto-card>
            </c-slide>
          </c-slider>
        </div>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
