<?php

use JRAPP\Controllers\AuthController;
use JRAPP\Controllers\SuscripcionController;
use JRAPP\Pages\ContactoPage;
use JRAPP\Pages\HomePage;
use JRAPP\Posttypes\PostType;
use JRAPP\Posttypes\IntencionType;
use JRAPP\Posttypes\IdeaType;
use JRAPP\Posttypes\ContactoType;
use JRAPP\Posttypes\PageType;
use JRAPP\Posttypes\PerfilType;
use JRAPP\Posttypes\ProyectoType;
use JRAPP\Util\GlobalData;
use JRAPP\Util\Init;

// error_reporting(E_ERROR | E_PARSE);
// definir directorios globales
// define('TEMPPATH', get_bloginfo('stylesheet_directory'));
define('THEME', 'jrapp');
define('VERSION', '1.2.0.1');
define('TEMPPATH', '/wp-content/themes/' . THEME . '');
define('MEDIA', TEMPPATH . '/assets/media');
define('JS', TEMPPATH . '/assets/js');
define('CSS', TEMPPATH . '/assets/css');
define('RUTA_ABSOLUTA', get_template_directory());
define('RUTA_RECURSOS', get_site_url() . '/wp-content/themes/' . THEME . '/assets');


require_once(RUTA_ABSOLUTA.'/app/utils/init.class.php');
require_once(RUTA_ABSOLUTA.'/app/utils/globaldata.class.php');
require_once(RUTA_ABSOLUTA.'/app/utils/util.class.php');
require_once(RUTA_ABSOLUTA . '/app/posttypes/user.class.php');

require_once(RUTA_ABSOLUTA.'/app/controllers/suscripcion.class.php');
require_once(RUTA_ABSOLUTA.'/app/controllers/auth.class.php');
require_once(RUTA_ABSOLUTA . '/app/posttypes/page.class.php');

require_once(RUTA_ABSOLUTA.'/app/pages/home.class.php');
require_once(RUTA_ABSOLUTA.'/app/posttypes/perfil.class.php');
require_once(RUTA_ABSOLUTA.'/app/posttypes/intencion.class.php');
require_once(RUTA_ABSOLUTA.'/app/pages/blog.class.php');
require_once(RUTA_ABSOLUTA . '/app/pages/proyecto.class.php');
require_once(RUTA_ABSOLUTA . '/app/posttypes/proyecto.class.php');
require_once(RUTA_ABSOLUTA . '/app/posttypes/post.class.php');
require_once(RUTA_ABSOLUTA.'/app/pages/contacto.class.php');
require_once(RUTA_ABSOLUTA.'/app/posttypes/contacto.class.php');
require_once(RUTA_ABSOLUTA . '/app/posttypes/idea.class.php');
require_once(RUTA_ABSOLUTA.'/app/pages/politicas.class.php');
require_once(RUTA_ABSOLUTA.'/app/pages/404.class.php');
require_once(RUTA_ABSOLUTA . '/app/pages/nosotros.class.php');


$init = new Init();
$CurrentUser = wp_get_current_user();
$Suscripcion = new SuscripcionController();
$Auth = new AuthController();
$Globaldata = new GlobalData();

// $Socket = new SocketController();
$contactoPage = new ContactoPage();

//PostTypes con suscripción
$Page = new PageType();
$homePage = new HomePage();
$Contacto = new ContactoType();
$Intencion = new IntencionType();
$Idea = new IdeaType();
$Post = new PostType();
$Proyecto = new ProyectoType();
$perfil = new PerfilType();
