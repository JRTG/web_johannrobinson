<?php

namespace JRAPP\Controllers;

class AuthController
{
  function __construct()
  {
    add_action('wp_ajax_login', array(&$this, 'login'));
    add_action('wp_ajax_nopriv_login', array(&$this, 'login'));

    add_action('wp_ajax_getCurrentUser', array(&$this, 'getCurrentUser'));
    add_action('wp_ajax_nopriv_getCurrentUser', array(&$this, 'getCurrentUser'));

    add_action('wp_ajax_logout', array(&$this, 'logout'));
    add_action('wp_ajax_nopriv_logout', array(&$this, 'logout'));
  }
  public function getCurrentUser()
  {
    echo json_encode(wp_get_current_user());
    die();
  }

  public function login()
  {
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $userSignOn = wp_signon($info, false);
    if (is_wp_error($userSignOn)) {
      echo json_encode([
        'loggedin' => false,
        'message' => __('Wrong username or password.')
      ]);
    } else {
      echo json_encode([
        'user' => [
          'ID' => $userSignOn->data->ID,
          'display_name' => $userSignOn->data->display_name,
          'avatar_url' => get_avatar_url($userSignOn->data->ID)
        ],
        'loggedin' => true,
        'message' => __('Login successful, redirecting...')
      ]);
    }
    die();
  }

  public function logout()
  {
    wp_logout();
    echo json_encode([
      'loggedin' => false,
      'message' => __('Closed Session')
    ]);
    die();
  }
}
