<?php
namespace JRAPP\Controllers;

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use WP_Query;
use JRAPP\Posttypes\UserType;

class SuscripcionController
{
    public $datos;

    function __construct()
    {
        add_action("wp_ajax_registrarSuscripcion", array(&$this, "registrarSuscripcion"));
        add_action("wp_ajax_nopriv_registrarSuscripcion", array(&$this, "registrarSuscripcion"));

        add_filter('manage_edit-suscripciones_columns', array(&$this, 'my_edit_suscripciones_columns'));
        add_action('manage_suscripciones_posts_custom_column', array(&$this, 'my_manage_suscripciones_columns'), 10, 2);
    }

    public function registrarSuscripcion()
    {
        // $subscription = json_decode(file_get_contents('php://input'), true);

        $subscription = $_POST;
        if (!isset($subscription['endpoint'])) {
            echo 'Error: not a subscription';
            return;
        }
        $method = $subscription['method'];
        $registro = $this->administrarSuscripcion($method);
        echo json_encode($registro);
        die();
    }
    public function administrarSuscripcion($action)
    {
        $subscription = $_POST;

        // revisamos si existe la suscripcion POST[endpoint]
        $args =   array(
            'post_type' => 'suscripciones',
            'meta_key'    => 'endpoint',
            'meta_value' => $subscription['endpoint']
        );

        $the_query = new WP_Query($args);

        switch ($action) {
            case 'PUT':
                $upd_post = array(
                    'ID'    => $the_query->post->ID,
                    'post_status'    => 'publish'
                );
                $post_id = wp_update_post($upd_post);

                update_field('field_5c0afd30ebaf4', $subscription['authToken'], $post_id);
                update_field('field_5c0affc4ebaf8', $subscription['publicKey'], $post_id);
                update_field('field_5c0afe7cebaf5', $subscription['contentEncoding'], $post_id);
                update_field('field_5c0afea4ebaf7', $subscription['method'], $post_id);

                $this->datos =  [
                    'estado'    =>  3,
                    'mensaje'      =>     'suscripción actualizada',
                    'subscription' =>  $subscription
                ];
                break;
            case 'POST':
                $userController = new UserType();
                // revisar si existe el usuario POST[email]

                $userData = [
                    'nombre' =>  $_POST['nombre'],
                    'email' =>  $_POST['email'],
                    'tel' => $_POST['tel']
                ];

                $user =  $userController->saveUser($userData);

                if (!$the_query->have_posts()) {
                    // $i = add_row( 'listado_de_Suscripciones', $subscription, $this->id );
                    if (!is_wp_error($user)) {
                        # code...
                        $new_post = array(
                            'post_title'    => get_user_by('id', $user)->data->user_email, //
                            'post_type'        => 'suscripciones',
                            'post_status'    => 'publish'
                        );
                        $post_id = wp_insert_post($new_post);

                        // regitramos la suscripción [idUser]
                        update_field('field_5e8bc59ae79cb', $user, $post_id);

                        update_field('field_5c0afe94ebaf6', $subscription['endpoint'], $post_id);
                        update_field('field_5c0afd30ebaf4', $subscription['authToken'], $post_id);
                        update_field('field_5c0affc4ebaf8', $subscription['publicKey'], $post_id);
                        update_field('field_5c0afe7cebaf5', $subscription['contentEncoding'], $post_id);
                        update_field('field_5c0afea4ebaf7', $subscription['method'], $post_id);
                        if ($post_id != 0) {
                            $listaDeNotificaciones = [
                                [
                                    'subscription' => Subscription::create([
                                        'endpoint' => $subscription['endpoint'], // Firefox 43+,
                                        'publicKey' => $subscription['publicKey'], // base 64 encoded, should be 88 chars
                                        'authToken' => $subscription['authToken'], // base 64 encoded, should be 24 chars
                                    ]),
                                    'payload' => json_encode([
                                        'title' => 'Suscripción Exitosa',
                                        'options' => [
                                            'body' => '¡Gracias por suscribirte!, Ahora déjanos tus datos aquí',
                                            'icon' => MEDIA . '/brand/256x256.png',
                                            'data' => [
                                                'url' => '/#home-wizard-news'
                                            ]
                                        ]
                                    ])
                                ]
                            ];
                            $this->enviarNotificaciones($listaDeNotificaciones);
                            $this->datos =  [
                                'estado'    =>  1,
                                'mensaje'      =>     'OK',
                                'subscription' =>  $subscription
                            ];
                        } else {
                            $this->datos =  [
                                'estado'    =>  2,
                                'mensaje'      =>     'Hubo un inconveniente'
                            ];
                        }
                    } else {
                        $this->datos =  [
                            'estado'    =>  5,
                            'mensaje'      =>  $user
                        ];
                    }
                }
                break;
            case 'DELETE':
                if ($the_query->have_posts()) {

                    $post_data = wp_delete_post($the_query->post->ID);

                    $this->datos =  [
                        'estado'    =>  4,
                        'mensaje'      =>     'Se eliminó tu suscripción',
                        'subscription' =>  $subscription
                    ];
                }
                break;
            default:
                $this->datos =  [
                    'estado'    =>  6,
                    'mensaje'      =>     'método no reconocido'
                ];
                break;
        }
        return $this->datos;
    }
    public function enviarNotificaciones($listaDeNotificaciones)
    {
        //Las suscripciones(authToken,endpoint,publicKey) serán guardadas en la BD para luego ser utilizadas
        $auth = array(
            'VAPID' => array(
                'subject' => 'mailto:a@a.com',
                'publicKey' => 'BEYMDSgBw9MfCb2NR420yoEQc1Srro1ftGWRfXpO3q5ge164bT9jHeCkb2gxg139tfKUkwRextgVBoUlHk-HKYM',
                'privateKey' => 'ESpFU7Y01ev4a4PDliM6W1NHjHLkaYNAU8iLHxXEztg', // in the real world, this would be in a secret file
            ),
        );
        $webPush = new WebPush($auth);
        foreach ($listaDeNotificaciones as $notificacion) {
            $webPush->sendNotification(
                $notificacion['subscription'],
                $notificacion['payload'], // optional (defaults null)
                true
            );
            foreach ($webPush->flush() as $report) {
                // $endpoint = $report->getRequest()->getUri()->__toString();
            }
        }
    }

    public function listarSuscripciones()
    {
        $listaSuscripciones = [];
        global $post;

        $paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;


        $args = array('orderby' => 'title', 'order' => 'ASC', 'post_type' => 'suscripciones', 'posts_per_page' => -1);

        $the_query = new WP_Query($args);
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $id         = get_the_ID();
                $authtoken    = get_field("authtoken");
                $contentencoding = get_field("contentencoding");

                $endpoint     = get_field("endpoint");
                $publickey     = get_field("publickey");

                $listaSuscripciones[] = [
                    'id'     => $id,
                    'authtoken'    => $authtoken,
                    'contentencoding'    => $contentencoding,
                    'endpoint'    => $endpoint,
                    'publickey'    => $publickey,
                ];
            }
        }
        return $listaSuscripciones;
    }

    public function my_edit_suscripciones_columns($columns)
    {

        $columns = array(
            'cb' => '&lt;input type="checkbox" />',
            'title' => __('Suscripciones'),
            'Usuario' => __('Usuario'),
            'date' => __('Date')
        );

        return $columns;
    }
    public function my_manage_suscripciones_columns($column, $post_id)
    {
        global $post;

        switch ($column) {

                /* If displaying the 'duration' column. */
            case 'Usuario':

                /* Get the post meta. */
                $User = get_post_meta($post_id, 'user', true);

                /* If no authToken is found, output a default message. */
                if (empty($User))
                    echo __('Unknown');

                /* If there is a User, append 'minutes' to the text string. */
                else
                    // printf(__('%s minutes'), $authToken);
                    $User = get_user_by('id', $User);
                echo ($User->data->user_email);

                break;

                /* If displaying the 'genre' column. */

            default:
                break;
        }
    }
}
