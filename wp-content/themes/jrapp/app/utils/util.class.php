<?php

namespace JRAPP\Util;

class Apariencia
{
  public static function construirMenu($tipo)
  {
    switch ($tipo) {
      case 1:
        wp_nav_menu(
          array(
            'theme_location' => 'menuPrincipal',
            'container' => 'nav',
            'container_class' => 'menu-nav',
            'menu_class' => 'menu-list u-flex u-flex-fw-w',
          )
        );
        break;

      default:
        # code...
        break;
    }
  }

  public static function formatearTexto($cadena)
  {
    $palabras = explode(' ', $cadena);
    $nroPalabras = count($palabras);
    $pxl1 = round($nroPalabras / 2);
    $pl1 = '';
    $pl2 = '';
    for ($i = 0; $i < $pxl1; $i++) {
      $pl1 = $pl1 . $palabras[$i] . ' ';
    }
    for ($j = $i; $j < $nroPalabras; $j++) {
      $pl2 = $pl2 . $palabras[$j] . ' ';
    }

    return [$pl1, $pl2];
  }

  public static function pipeText($texto, $caracter = '|')
  {
    $arrParts = explode($caracter, $texto);
    for ($i = 0; $i < count($arrParts); $i++) {
      $arrParts[$i] = trim($arrParts[$i]);
    }
    return $arrParts;
  }

  public static function obtenerTemplate($template, $data)
  {
    $q = file_get_contents(RUTA_ABSOLUTA . '/templates/' . $template . '.template.html');
    foreach ($data as $key => $value) {
      $q = str_replace('{' . $key . '}', $value, $q);
    }
    return $q;
  }

  public static function colorLuminance($color, $opacity = false)
  {
    $default = 'rgb(0,0,0)';

    if (empty($color))
      return $default;

    if ($color[0] == '#')
      $color = substr($color, 1);

    if (strlen($color) == 6)
      $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);

    elseif (strlen($color) == 3)
      $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);

    else
      return $default;

    $rgb = array_map('hexdec', $hex);

    if ($opacity) {
      if (abs($opacity) > 1)
        $opacity = 1.0;

      $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
    } else {
      $output = 'rgb(' . implode(",", $rgb) . ')';
    }
    return $output;
  }
}
