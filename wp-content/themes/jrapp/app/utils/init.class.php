<?php

namespace JRAPP\Util;

use WP_Error;

class Init
{
  function __construct()
  {
    // Luego de instalado el Theme Jrapp
    add_action('after_setup_theme', array(&$this, 'afterSetupTheme'));

    // Customizando el Panel de Administración
    add_action('login_head', array(&$this, 'customAdmin'));
    add_action('admin_head', array(&$this, 'customAdmin'));
    add_filter('admin_footer_text', array(&$this,'footerAdmin'));

    // [Intersectar] Subida de archivo validando su tamaño
    add_filter('upload_size_limit', array(&$this, 'uploadSizeLimit'));
    add_filter('wp_handle_upload_prefilter', array(&$this, 'handleUploadPreFilter'), 10, 1);

    // Setear el Key de Google Maps a partir de un option
    add_filter('acf/settings/google_api_key', array(&$this, 'getKeyMaps'));

    add_action('admin_init', array(&$this, 'adminInit'));

    // Proteger el API REST
    add_filter( 'rest_authentication_errors', function( $result ) {
      if ( ! empty( $result ) ) {
        return $result;
      }
      if ( ! is_user_logged_in() ) {
        return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
      }
      return $result;
    });
  }

  public function afterSetupTheme()
  {
    if (function_exists('add_theme_support')) {
      add_theme_support('woocommerce');
      add_theme_support('post-thumbnails');
      add_image_size('default', 0, 0, true);

      add_post_type_support('page', 'excerpt');
      add_post_type_support('perfiles', 'excerpt');
      add_post_type_support('proyectos', 'excerpt');
      add_post_type_support('intenciones', 'excerpt');


      add_theme_support(
        'post-formats',
        array('aside', 'image', 'link', 'quote', 'gallery', 'status', 'video', 'audio')
      );
    }

    $this->registerMenus();
  }

  public function handleUploadPreFilter($file)
  {
    $size = $file['size'];
    if ($size > 200 * 1024) {
      $file['error'] = __('Máximo 200kb para archivos Multimedia', 'textdomain');
    }
    return $file;
  }

  public function uploadSizeLimit()
  {
    return 200 * 1024;
  }

  public function customAdmin()
  {
    echo  '<link rel="icon" type="image/png" href="' . MEDIA . '/brand/256x256.png"/>
          <link rel="apple-touch-icon-precomposed" sizes="114x114" href="' . MEDIA . '/brand/114x114.png">
          <link rel="apple-touch-icon-precomposed" sizes="72x72" href="' . MEDIA . '/brand/72x72.png">
          <link rel="apple-touch-icon-precomposed" href="' . MEDIA . '/brand/57x57.png">
          <link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/assets/css/styles-admin.css?v=0.74">';
  }

  public function footerAdmin()
  {
    echo '&copy;2011 Copyright Johann Robinson. Todos los derechos reservados - Powered by <a href="https://johannrobinson.com/" target="_blank" class="fa-dev icon-logo-johann"></img></a>';
  }

  public function registerMenus()
  {
    if (function_exists('register_nav_menus')) {
      register_nav_menus(
        array(
          'menuPrincipal' => 'Menu del header'
        )
      );
    }
  }

  public function getKeyMaps()
  {
    return get_option('Maps');
  }
  public function adminInit()
  {
    if (!current_user_can('manage_options') && is_user_logged_in()) {
      // remove_menu_page('edit.php'); // Entradas de blog
      //remove_menu_page('upload.php'); // Multimedia
      remove_menu_page('link-manager.php'); // Enlaces
      //remove_menu_page('edit.php?post_type=page'); // Páginas
      remove_menu_page('edit.php?post_type=suscripciones'); // suscripciones
      remove_menu_page('edit-comments.php'); // Comentarios
      remove_menu_page('themes.php'); // Apariencia
      remove_menu_page('plugins.php'); // Plugins
      remove_menu_page('users.php'); // Usuarios
      remove_menu_page('tools.php'); // Herramientas
      remove_menu_page('options-general.php'); // Ajustes
    }
  }
}
