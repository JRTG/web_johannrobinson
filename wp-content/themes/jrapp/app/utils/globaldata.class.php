<?php

namespace JRAPP\Util;

class GlobalData
{
  function __construct()
  {
    add_filter('admin_init', array(&$this, 'register_fields'));
  }
  function register_fields()
  {
    register_setting('general', 'Google_Analytics', 'esc_attr');
    add_settings_field('Google_Analytics', '<label for="Google_Analytics">Google Analytics Code</label>', array(&$this, 'fields_GA'), 'general');

    register_setting('general', 'Mant', 'esc_attr');
    add_settings_field('Mant', '<label for="Mant">Activar Maintenance Mode</label>', array(&$this, 'fields_Mant'), 'general');

    register_setting('general', 'Deploy', 'esc_attr');
    add_settings_field('Deploy', '<label for="Deploy">Fecha de Deploy Producción</label>', array(&$this, 'fields_Deploy'), 'general');

    register_setting('general', 'Maps', 'esc_attr');
    add_settings_field('Maps', '<label for="Maps">Id Api Maps</label>', array(&$this, 'fields_Maps'), 'general');

    register_setting('general', 'Keywords', 'esc_attr');
    add_settings_field('Keywords', '<label for="Keywords">Palabras Clave</label>', array(&$this, 'fields_Keywords'), 'general');
  }

  function fields_GA()
  {
    $value = get_option('Google_Analytics', '');
    echo '<input type="text" id="Google_Analytics" name="Google_Analytics" value="' . $value . '" />';
  }
  function fields_Mant()
  {
    $value = get_option('Mant', '');
    echo '<input type="checkbox" id="Mant" name="Mant" ' . ($value != '' ? 'checked' : '') . ' value="SI" />';
  }
  function fields_Deploy()
  {
    $value = get_option('Deploy', '');
    echo '<input type="date" id="Deploy" name="Deploy" value="' . $value . '" />';
  }
  function fields_Maps()
  {
    $value = get_option('Maps', '');
    echo '<input type="text" id="Maps" name="Maps" value="' . $value . '" />';
  }
  function fields_Keywords()
  {
    $value = get_option('Keywords', '');
    echo '<input type="text" id="Keywords" name="Keywords" value="' . $value . '" />';
  }
}
