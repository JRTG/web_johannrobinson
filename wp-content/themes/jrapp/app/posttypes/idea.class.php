<?php

namespace JRAPP\Posttypes;

use JRAPP\Posttypes\UserType;
use WP_Query;

class IdeaType
{
  function __construct()
  {
    add_action("wp_ajax_registrarIdeaAjax", array(&$this, "registrarIdeaAjax"));
    add_action("wp_ajax_nopriv_registrarIdeaAjax", array(&$this, "registrarIdeaAjax"));

    add_filter('manage_edit-ideas_columns', array(&$this, 'my_edit_ideas_columns'));
    add_action('manage_ideas_posts_custom_column', array(&$this, 'my_manage_ideas_columns'), 10, 2);
  }

  public function listarIdeasAjax()
  {
    $listaIdeas = $this->listarIdeas($_POST['query'], false);
    echo json_encode($listaIdeas);
    die();
  }
  public function listarIdeas($args = [], $paginacion)
  {
    $datos = [
      'listaIdeas' => [],
      'pager'             => ''
    ];

    $args['post_type'] = 'ideas';

    if ($paginacion) {
      $paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
      $args['paged'] = $paged;
    }

    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id         = get_the_ID();
        $title         = get_the_title();
        $descripcion    = get_field('descripcion');
        $perfil    = get_field('perfil');

        $datos['listaIdeas'][] = [
          'id'    => $id,
          'title'    => $title,
          'descripcion'    => $descripcion,
          'perfil'    => $perfil
        ];
      }

      if ($paginacion) {
        $defaults = array(
          'base'                    => get_bloginfo('url') . '/proyectos/%_%',
          'format'                => '?page=%#%',
          'current'                => max(1, get_query_var('page')),
          'total'                    => $the_query->max_num_pages,
          'mid_size'                => 2,
          'type'                    => 'list'
        );
        if ($the_query->max_num_pages > 1) {
          $datos['pager'] = '<div class="g-page"><ul>' . paginate_links($defaults) . '</ul></div>';
        }
      }

      return $datos;
    } else {
      return $datos['listaIdeas'] = [];
    }
  }

  public function registrarIdeaAjax()
  {
    $idea = $_POST['idea'];
    $datos = [];
    $userController = new UserType();
    $user = $userController->saveUser($idea['user']);
    if (!is_wp_error($user)) {
      $rIdea = $this->registrarIdea([
        'user'   => $user,
        'intencion' => $idea['intencion'],
        'perfil'    => $idea['perfil'],
        'descripcion' => $idea['descripcion']
      ]);
      $datos = $rIdea;
    } else {
      $datos = [
        'estado' => 3,
        'mensaje' => 'Hubo un problema al registar el Usuario'
      ];
    }

    echo json_encode($datos);
    die();
  }

  public function registrarIdea($idea)
  {
    $datos = [];
    $titleIntencion = (new IntencionType())->listarIntenciones(['p' => $idea['intencion']], false)['listaIntenciones'][0]['title'];
    $post_id = wp_insert_post([
      'post_title'    => $titleIntencion,
      'post_type'        => 'ideas',
      'post_status'    => 'publish'
    ]);

    if ($post_id != 0) {
      # code...
      update_field('field_5e98b8efeef8e', $idea['user'], $post_id);
      update_field('field_5e98b956eef8f', $idea['intencion'], $post_id);
      update_field('field_5e98b9bbeef90', $idea['perfil'], $post_id);
      update_field('field_5e98b9d3eef91', $idea['descripcion'], $post_id);
      $datos = [
        'estado' => 1,
        'mensaje' => 'Idea registrada',
        'idea' => $idea
      ];
    } else {
      $datos = [
        'estado' => 2,
        'mensaje' => 'Hubo un problema al registrar'
      ];
    }
    return $datos;
  }

  public function my_edit_ideas_columns($columns)
  {

    $columns = array(
      'cb' => '&lt;input type="checkbox" />',
      'title' => __('Ideas'),
      'Usuario' => __('Usuario'),
      'Perfil' => __('Perfil'),
      'Desc' => __('Desc'),
      'date' => __('Date')
    );

    return $columns;
  }
  public function my_manage_ideas_columns($column, $post_id)
  {
    global $post;

    switch ($column) {

        /* If displaying the 'duration' column. */
      case 'Usuario':
        $User = get_post_meta($post_id, 'user', true);
        if (empty($User))
          echo __('Unknown');
        else
          $User = get_user_by('id', $User);
        echo ($User->data->user_email);

        break;

      case 'Perfil':
        $Perfil = get_post_meta($post_id, 'perfil', true);
        if (empty($Perfil))
          echo __('Unknown');
        else
          $Perfil = get_the_title($Perfil);
        echo ($Perfil);
        break;
      case 'Desc':
        $Desc = get_post_meta($post_id, 'descripcion', true);
        if (empty($Desc))
          echo __('Unknown');
        else
          echo ($Desc);
        break;

        /* If displaying the 'genre' column. */

      default:
        break;
    }
  }
}
