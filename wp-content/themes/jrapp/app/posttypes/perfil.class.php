<?php

namespace JRAPP\Posttypes;

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use WP_Query;
use JRAPP\Controllers\SuscripcionController;

class PerfilType
{
  public $datos;
  function __construct()
  {
    add_action("wp_ajax_listarPerfilesAjax", array(&$this, "listarPerfilesAjax"));
    add_action("wp_ajax_nopriv_listarPerfilesAjax", array(&$this, "listarPerfilesAjax"));
    // REF: https://developer.wordpress.org/reference/hooks/save_post_post-post_type/
    add_action('save_post_perfiles', array(&$this, "afterSavePerfiles"), 10, 3);
  }
  public function listarPerfilesAjax()
  {
    $listaPerfiles = $this->listarPerfiles($_POST['query'], false);
    echo json_encode($listaPerfiles);
    die();
  }
  public function listarPerfiles($args = [], $paginacion)
  {
    $datos = [
      'listaPerfiles' => [],
      'pager'             => ''
    ];

    $args['post_type'] = 'perfiles';

    if ($paginacion) {
      $paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
      $args['paged'] = $paged;
    }

    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id         = get_the_ID();
        $title         = get_the_title();
        $pm         = get_the_permalink();
        $imagen        = get_field('imagen');
        $descripcion         = htmlspecialchars(get_field("descripcion"));
        $extracto = get_the_excerpt();

        $datos['listaPerfiles'][] = [
          'id'    => $id,
          'title'    => $title,
          'pm'    => $pm,
          'imagen' => $imagen,
          'descripcion' => $descripcion,
          'extracto' => $extracto
        ];
      }

      if ($paginacion) {
        $defaults = array(
          'base'                    => get_bloginfo('url') . '/perfiles/%_%',
          'format'                => '?page=%#%',
          'current'                => max(1, get_query_var('page')),
          'total'                    => $the_query->max_num_pages,
          'mid_size'                => 2,
          'type'                    => 'list'
        );
        if ($the_query->max_num_pages > 1) {
          $datos['pager'] = '<div class="g-page"><ul>' . paginate_links($defaults) . '</ul></div>';
        }
      }

      return $datos;
    } else {
      return $datos['listaPerfiles'] = [];
    }
  }
  public function obtenerPerfil($id = null)
  {

    $id = is_null($id) ? get_the_ID() : $id;
    $args = array('p' => $id, 'post_type' => 'perfiles');
    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id         = get_the_ID();
        $title         = get_the_title();
        $pm         = get_the_permalink();
        $imagen        = get_field('imagen');
        $descripcion         = get_field("descripcion");
        $extracto = get_the_excerpt();

        $datos = [
          'id'    => $id,
          'title'    => $title,
          'pm'    => $pm,
          'imagen' => $imagen,
          'descripcion' => $descripcion,
          'extracto' => $extracto
        ];
      }

      return $datos;
    } else {
    }
  }

  public function afterSavePerfiles($post_id, $post, $update)
  {
    if ($post->post_status == 'auto-draft' || $post->post_status == 'draft') {
      return;
    }

    $update = ($post->post_date != $post->post_modified); //redefinimos $update

    $notPerfiles = new SuscripcionController;
    $notificados = $notPerfiles->listarSuscripciones();
    $listaDeNotificaciones = [];

    foreach ($notificados as  $notificado) {
      $listaDeNotificaciones[] =     [
        'subscription' => Subscription::create([
          'endpoint' => $notificado['endpoint'], // Firefox 43+,
          'publicKey' => $notificado['publickey'], // base 64 encoded, should be 88 chars
          'authToken' => $notificado['authtoken'], // base 64 encoded, should be 24 chars
        ]),
        'payload' => json_encode([
          'title' => 'Johann Robinson',
          'options' => [
            'body' => ($update ? 'Renovamos el Perfil de ' . get_the_title($post_id) . ', ¡Mira aquí!' : '¡Hola!, Un nuevo Perfil académico para tus ti: ' . get_the_title($post_id) . ', ¡Entérate!'),
            'icon' => MEDIA . '/brand/256x256.png',
            'image' => get_field("foto", $post_id)["url"],
            'data' => [
              'url' => get_permalink($post_id) . "?v=" . time()
            ]
          ]
        ])
      ];
    }

    $notPerfiles->enviarNotificaciones($listaDeNotificaciones);
    return true;
  }
}
