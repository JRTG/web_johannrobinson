<?php
namespace JRAPP\Posttypes;

use \Firebase\JWT\JWT;
use JRAPP\Util\Apariencia;
use WP_Query;

class ContactoType
{
  function __construct()
  {

    add_action("wp_ajax_agregarContacto", array(&$this, "agregarContacto"));
    add_action("wp_ajax_nopriv_agregarContacto", array(&$this, "agregarContacto"));

    // REF: https://developer.wordpress.org/reference/hooks/save_post_post-post_type/
    add_action('save_post_suscriptores', array(&$this, "afterSaveSuscriptores"), 10, 3);
  }

  public function agregarContacto()
  {
    $suscriptor = array(
      'nombres_y_apellidos'   => $_POST['nomape'],
      'telefono'              => $_POST['tel'],
      'correo_electronico'    => $_POST['email'],
      'mensaje'               => $_POST['mensaje'],
      'opcion'               => $_POST['opcion']
    );

    $dataMail = array(
      'userName' => $suscriptor['nombres_y_apellidos'],
      'userMail' => $suscriptor['correo_electronico'],
      'adminMail' => get_option('admin_email'),
      'srcMedia' => RUTA_RECURSOS . '/css',
      'linkDomain' => get_option('siteurl'),
      'políticas' => 'Nuestras Políticas de uso de datos'
    );
    $domain = explode('@', $suscriptor['correo_electronico'])[1];
    if (checkdnsrr($domain, 'MX')) {
      $args =   array(
        'post_type' => 'suscriptores',
        'meta_key'  => 'correo_electronico',
        'meta_value' => $suscriptor['correo_electronico']
      );

      $the_query = new WP_Query($args);
      if (!$the_query->have_posts()) {
        // $i = add_row( 'listado_de_suscriptores', $suscriptor, $this->id );
        $new_post = array(
          'post_title'  => $suscriptor['correo_electronico'],
          'post_type'    => 'suscriptores',
          'post_status'  => 'publish'
        );
        $post_id = wp_insert_post($new_post);

        update_field('field_5ae0112f2916f', $suscriptor['nombres_y_apellidos'], $post_id);
        update_field('field_5ae0114829170', $suscriptor['telefono'], $post_id);
        update_field('field_5ae0116229171', $suscriptor['correo_electronico'], $post_id);
        update_field('field_5ae011b029172', $suscriptor['mensaje'], $post_id);
        update_field('field_5ae011e429173', $suscriptor['opcion'], $post_id);
        if ($post_id != 0) {
          $to = $suscriptor['correo_electronico'];
          $subject = 'Hola, ' . $suscriptor['nombres_y_apellidos'] . ' te has registrado';
          $body = Apariencia::obtenerTemplate('thanks', $dataMail);
          $headers = array('Content-Type: text/html; charset=UTF-8');

          wp_mail($to, $subject, $body, $headers);

          $this->datos =  [
            'estado'    =>  1,
            'mensaje'    =>   'OK',
            'suscriptor' =>  $suscriptor
          ];
        } else {
          $this->datos =  [
            'estado'    =>  2,
            'mensaje'    =>   'Hubo un inconveniente'
          ];
        }
      } else {
        $this->datos =  [
          'estado'    =>  3,
          'mensaje'    =>   'Ya se ha registrado'
        ];
      }
    } else {
      $this->datos =  [
        'estado'    =>  2,
        'mensaje'    =>   'El dominio del correo ingresado no existe'
      ];
    }

    echo (json_encode($this->datos));
    die();
  }

  public function afterSaveSuscriptores($post_id, $post, $update)
  {
    /*
            * In production code, $slug should be set only once in the plugin,
            * preferably as a class property, rather than in each function that needs it.
            */
    $post_type = get_post_type($post_id);


    // If this isn't a 'book' post, don't update it.
    return true;
  }
}
