<?php
namespace JRAPP\Posttypes;

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use JRAPP\Controllers\SuscripcionController;

class PageType
{
  function __construct()
  {
    add_action('save_post_page', array(&$this, "afterSavePage"), 10, 3);
  }

  public function afterSavePage($post_id, $post, $update)
  {
    if ($post->post_status == 'auto-draft' || $post->post_status == 'draft') {
      return;
    }

    $update = ($post->post_date != $post->post_modified); //redefinimos $update

    $notificador = new SuscripcionController;
    $notificados = $notificador->listarSuscripciones();
    $listaDeNotificaciones = [];

    foreach ($notificados as  $notificado) {
      $listaDeNotificaciones[] =     [
        'subscription' => Subscription::create([
          'endpoint' => $notificado['endpoint'], // Firefox 43+,
          'publicKey' => $notificado['publickey'], // base 64 encoded, should be 88 chars
          'authToken' => $notificado['authtoken'], // base 64 encoded, should be 24 chars
        ]),
        'payload' => json_encode([
          'title' => get_bloginfo('name'),
          'options' => [
            'body' => ($update ? 'Nuevo contenido en ' . $post->post_title . ', ¡Mira aquí!' : '¡Hola!, hemos lanzado ' . $post->post_title . ', ¡Entérate!'),
            'icon' => MEDIA . '/brand/256x256.png',
            'image' => get_the_post_thumbnail_url($post),
            'data' => [
              'url' => get_permalink($post_id) . "?v=" . time()
            ]
          ]
        ])
      ];
    }

    $notificador->enviarNotificaciones($listaDeNotificaciones);
    return true;
  }

  public function get_id_by_slug($page_slug)
  {
    $page = get_page_by_path($page_slug);
    if ($page) {
      return $page->ID;
    } else {
      return null;
    }
  }
}
