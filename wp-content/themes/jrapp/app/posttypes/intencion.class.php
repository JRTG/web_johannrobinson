<?php
namespace JRAPP\Posttypes;

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use WP_Query;
use JRAPP\Controllers\SuscripcionController;

class IntencionType
{
	function __construct()
	{
		add_action("wp_ajax_listarIntencionesAjax", array(&$this, "listarIntencionesAjax"));
		add_action("wp_ajax_nopriv_listarIntencionesAjax", array(&$this, "listarIntencionesAjax"));

		// REF: https://developer.wordpress.org/reference/hooks/save_post_post-post_type/
		add_action('save_post_intenciones', array(&$this, "afterSaveIntenciones"), 10, 3);
	}

	public function listarIntenciones($args, $paginacion)
	{
		$datos = [
			'listaIntenciones' => [],
			'pager'			 => ''
		];

		$args['post_type'] = 'intenciones';

		if ($paginacion) {
			$paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
			$args['paged'] = $paged;
		}

		$the_query = new WP_Query($args);
		$str = '';
		if ($the_query->have_posts()) {
			while ($the_query->have_posts()) {
				$the_query->the_post();
				$id 		= get_the_ID();
				$title 		= get_the_title();
				$pm 		= get_the_permalink();
				$imagen		= get_field('imagen');
				$banner 		= get_field("banner");
				$contenido 		= get_field("contenido");
				$perfiles 		= get_field("perfiles");

				$datos['listaIntenciones'][] = [
					'id'	=> $id,
					'title'	=> $title,
					'pm'	=> $pm,
					'imagen' => $imagen,
					'banner' => $banner,
					'contenido' => $contenido,
					'perfiles' => $perfiles
				];
			}

			if ($paginacion) {
				$defaults = array(
					'base'					=> get_bloginfo('url') . '/intenciones/%_%',
					'format'				=> '?page=%#%',
					'current'				=> max(1, get_query_var('page')),
					'total'					=> $the_query->max_num_pages,
					'mid_size'				=> 2,
					'type'					=> 'list'
				);
				if ($the_query->max_num_pages > 1) {
					$datos['pager'] = '<div class="g-page"><ul>' . paginate_links($defaults) . '</ul></div>';
				}
			}


		} else {
			$datos['mensaje'] = 'No hay intenciones en este momento';
		}
		return $datos;
	}
	public function listarIntencionesAjax(){
		$listaIntenciones = $this->listarIntenciones($_POST['query'],false);
		echo json_encode($listaIntenciones);
		die();
	}
	public function obtenerIntencion($id=null)
	{

		$id = is_null($id) ? get_the_ID() : $id;
		$args = array('p' => $id, 'post_type' => 'intenciones');
		$the_query = new WP_Query($args);
		$str = '';
		if ($the_query->have_posts()) {
			while ($the_query->have_posts()) {
				$the_query->the_post();
				$id 		= get_the_ID();
				$title 		= get_the_title();
				$pm 		= get_the_permalink();
				$imagen		= get_field('imagen');
				$banner 		= get_field("banner");
				$contenido 		= get_field("contenido");
				$perfiles 		= get_field("perfiles");

				$datos = [
					'id'	=> $id,
					'title'	=> $title,
					'pm'	=> $pm,
					'imagen' => $imagen,
					'banner' => $banner,
					'contenido' => $contenido,
					'perfiles' => $perfiles
				];
			}

			return $datos;
		} else {
		}
	}

	public function afterSaveIntenciones($post_id, $post, $update)
	{
		if($post->post_status =='auto-draft' || $post->post_status == 'draft'){
			return;
		}

		$update = ($post->post_date != $post->post_modified); //redefinimos $update

		$notIntenciones = new SuscripcionController;
		$notificados = $notIntenciones->listarSuscripciones();
		$listaDeNotificaciones = [];

		foreach ($notificados as  $notificado) {
			$listaDeNotificaciones[] = 	[
				'subscription' => Subscription::create([
					'endpoint' => $notificado['endpoint'], // Firefox 43+,
					'publicKey' => $notificado['publickey'], // base 64 encoded, should be 88 chars
					'authToken' => $notificado['authtoken'], // base 64 encoded, should be 24 chars
				]),
				'payload' => json_encode([
					'title' => get_the_title($post_id),
					'options' => [
						'body' => ($update ? 'Renovamos la Intencion de ' . get_the_title($post_id) . ', ¡Mira aquí!' : '¡Hola!, Un nuevo Intencion académico para tus ti: ' . get_the_title($post_id) . ', ¡Entérate!'),
						'icon' => MEDIA . '/brand/256x256.png',
						'image' => get_the_post_thumbnail_url($post),
						'data' => [
							'url' => get_permalink($post_id) . "?v=" . time()
						]
					]
				])
			];
		}

		$notIntenciones->enviarNotificaciones($listaDeNotificaciones);
		return true;
	}
}
