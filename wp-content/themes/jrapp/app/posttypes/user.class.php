<?php
namespace JRAPP\Posttypes;

class UserType
{
    function __construct()
    {
    }

    public function saveUser($user)
    {
        $userId = email_exists($user['email']);

        if (!$userId) {
            // registrar el usuario (retorna el id de usuario)
            $userId = wp_insert_user([
                'user_login' =>  $user['nombre'],
                'user_email' =>  $user['email'],
                'user_pass'  =>  'A$W&S12121121aa'
            ]);
            update_user_meta($userId, 'billing_phone', $user['tel']);
        }else{
            update_user_meta($userId, 'billing_phone', $user['tel']);
        }

        return $userId;
    }

    public function getUser($userId){
        $userData = get_userdata($userId);
        return $userData;
    }

}
