<?php

namespace JRAPP\Posttypes;

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use WP_Query;
use JRAPP\Controllers\SuscripcionController;

class PostType
{
  function __construct()
  {
    add_action("wp_ajax_listarPostsAjax", array(&$this, "listarPostsAjax"));
    add_action("wp_ajax_nopriv_listarPostsAjax", array(&$this, "listarPostsAjax"));

    add_action('save_post_post', array(&$this, "afterSavePosts"), 10, 3);
  }

  public function listarPostsAjax()
  {
    $listaPosts = $this->listarPosts($_POST['query'], false);
    echo json_encode($listaPosts);
    die();
  }
  public function listarPosts($args = [], $paginacion)
  {
    $datos = [
      'listaPosts' => [],
      'pager'             => ''
    ];

    $args['post_type'] = 'post';

    if ($paginacion) {
      $paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
      $args['paged'] = $paged;
    }

    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id     = get_the_ID();
        $title     = get_the_title();
        $categoria = get_the_category();
        $date = get_the_date();
        $extracto = get_the_excerpt();
        $pm     = get_the_permalink();
        $banner     = get_field("banner");
        $contenido     = get_field("contenido");

        $datos['listaPosts'][] = [
          'id'   => $id,
          'title'  => $title,
          'categoria' => $categoria,
          'date' => $date,
          'pm'  => $pm,
          'banner' => $banner,
          'contenido' => $contenido,
          'extracto' => $extracto
        ];
      }

      if ($paginacion) {
        $defaults = array(
          'base'          => get_bloginfo('url') . '/Posts/%_%',
          'format'        => '?page=%#%',
          'current'        => max(1, get_query_var('page')),
          'total'          => $the_query->max_num_pages,
          'mid_size'        => 2,
          'type'          => 'list'
        );
        if ($the_query->max_num_pages > 1) {
          $datos['pager'] = '<div class="g-page"><ul>' . paginate_links($defaults) . '</ul></div>';
        }
      }

      return $datos;
    } else {
      return $datos['listaPosts'][] = '<p>No se encontraron Posts disponibles</p>';
    }
  }

  public function obtenerPost($id = null)
  {
    $id = is_null($id) ? get_the_ID() : $id;
    $args = array('p' => $id, 'post_type' => 'post');
    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id     = get_the_ID();
        $title     = get_the_title();
        $categoria = get_the_category();
        $date = get_the_date();
        $extracto = get_the_excerpt();
        $pm     = get_the_permalink();
        $banner     = get_field("banner");
        $contenido     = get_field("contenido");

        $datos = [
          'id'  => $id,
          'title'  => $title,
          'categoria' => $categoria,
          'date' => $date,
          'pm'  => $pm,
          'banner' => $banner,
          'contenido' => $contenido,
          'extracto' => $extracto
        ];
      }

      return $datos;
    } else {
    }
  }

  public function afterSavePosts($post_id, $post, $update)
  {
    if ($post->post_status == 'auto-draft' || $post->post_status == 'draft') {
      return;
    }

    $update = ($post->post_date != $post->post_modified); //redefinimos $update

    $notPosts = new SuscripcionController;
    $notificados = $notPosts->listarSuscripciones();
    $listaDeNotificaciones = [];

    foreach ($notificados as  $notificado) {
      $listaDeNotificaciones[] =   [
        'subscription' => Subscription::create([
          'endpoint' => $notificado['endpoint'], // Firefox 43+,
          'publicKey' => $notificado['publickey'], // base 64 encoded, should be 88 chars
          'authToken' => $notificado['authtoken'], // base 64 encoded, should be 24 chars
        ]),
        'payload' => json_encode([
          'title' => get_bloginfo('name'),
          'options' => [
            'body' => ($update ? 'Renovamos el Post de ' . get_the_title($post_id) . ', ¡Mira aquí!' : '¡Hola!, Un nuevo Post para ti: ' . get_the_title($post_id) . ', ¡Entérate!'),
            'icon' => MEDIA . '/brand/256x256.png',
            'image' => get_the_post_thumbnail_url($post),
            'data' => [
              'url' => get_permalink($post_id) . "?v=" . time()
            ]
          ]
        ])
      ];
    }

    $notPosts->enviarNotificaciones($listaDeNotificaciones);
    return true;
  }
}
