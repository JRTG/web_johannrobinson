<?php

namespace JRAPP\Posttypes;

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use WP_Query;
use JRAPP\Controllers\SuscripcionController;

class ProyectoType
{
  function __construct()
  {
    add_action("wp_ajax_listarProyectosAjax", array(&$this, "listarProyectosAjax"));
    add_action("wp_ajax_nopriv_listarProyectosAjax", array(&$this, "listarProyectosAjax"));
    // REF: https://developer.wordpress.org/reference/hooks/save_post_post-post_type/
    add_action('save_post_proyectos', array(&$this, "afterSaveProyectos"), 10, 3);
  }
  public function listarProyectosAjax()
  {
    $listaProyectos = $this->listarProyectos($_POST['query'], false);
    echo json_encode($listaProyectos);
    die();
  }
  public function listarProyectos($args = [], $paginacion)
  {
    $datos = [
      'listaProyectos' => [],
      'pager'             => ''
    ];

    $args['post_type'] = 'proyectos';

    if ($paginacion) {
      $paged = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
      $args['paged'] = $paged;
    }

    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id         = get_the_ID();
        $title         = get_the_title();
        $pm         = get_the_permalink();
        $banner      = get_field('banner');
        $empresa    = get_field('empresa');
        $prototipo  = get_field('prototipo');
        $contenido     = get_field("contenido");
        $color = get_field('color');
        $categoria = get_the_terms($id, 'categoria');
        $extracto = get_the_excerpt();

        $datos['listaProyectos'][] = [
          'id'    => $id,
          'title'    => $title,
          'pm'    => $pm,
          'banner' => $banner,
          'empresa' => $empresa,
          'prototipo' => $prototipo,
          'contenido' => $contenido,
          'categoria' => $categoria,
          'color' => $color,
          'extracto' => $extracto
        ];
      }

      if ($paginacion) {
        $defaults = array(
          'base'                    => get_bloginfo('url') . '/proyectos/%_%',
          'format'                => '?page=%#%',
          'current'                => max(1, get_query_var('page')),
          'total'                    => $the_query->max_num_pages,
          'mid_size'                => 2,
          'type'                    => 'list'
        );
        if ($the_query->max_num_pages > 1) {
          $datos['pager'] = '<div class="g-page"><ul>' . paginate_links($defaults) . '</ul></div>';
        }
      }

      return $datos;
    } else {
      return $datos['listaProyectos'] = [];
    }
  }
  public function obtenerProyecto($id = null)
  {

    $id = is_null($id) ? get_the_ID() : $id;
    $args = array('p' => $id, 'post_type' => 'proyectos');
    $the_query = new WP_Query($args);
    $str = '';
    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) {
        $the_query->the_post();
        $id         = get_the_ID();
        $title         = get_the_title();
        $pm         = get_the_permalink();
        $banner      = get_field('banner');
        $empresa    = get_field('empresa');
        $prototipo  = get_field('prototipo');
        $contenido     = get_field("contenido");
        $color = get_field('color');
        $categoria = get_the_terms($id, 'categoria');
        $extracto = get_the_excerpt();

        $datos = [
          'id'    => $id,
          'title'    => $title,
          'pm'    => $pm,
          'banner' => $banner,
          'empresa' => $empresa,
          'prototipo' => $prototipo,
          'contenido' => $contenido,
          'categoria' => $categoria,
          'color' => $color,
          'extracto' => $extracto
        ];
      }

      return $datos;
    } else {
    }
  }

  public function afterSaveProyectos($post_id, $post, $update)
  {
    if ($post->post_status == 'auto-draft' || $post->post_status == 'draft') {
      return;
    }

    $update = ($post->post_date != $post->post_modified); //redefinimos $update

    $notProyectos = new SuscripcionController;
    $notificados = $notProyectos->listarSuscripciones();
    $listaDeNotificaciones = [];

    foreach ($notificados as  $notificado) {
      $listaDeNotificaciones[] =     [
        'subscription' => Subscription::create([
          'endpoint' => $notificado['endpoint'], // Firefox 43+,
          'publicKey' => $notificado['publickey'], // base 64 encoded, should be 88 chars
          'authToken' => $notificado['authtoken'], // base 64 encoded, should be 24 chars
        ]),
        'payload' => json_encode([
          'title' => 'Johann Robinson',
          'options' => [
            'body' => ($update ? 'Renovamos el Proyecto de ' . get_the_title($post_id) . ', ¡Mira aquí!' : '¡Hola!, Un nuevo Proyecto académico para tus ti: ' . get_the_title($post_id) . ', ¡Entérate!'),
            'icon' => MEDIA . '/brand/256x256.png',
            'image' => get_field("foto", $post_id)["url"],
            'data' => [
              'url' => get_permalink($post_id) . "?v=" . time()
            ]
          ]
        ])
      ];
    }

    $notProyectos->enviarNotificaciones($listaDeNotificaciones);
    return true;
  }
}
