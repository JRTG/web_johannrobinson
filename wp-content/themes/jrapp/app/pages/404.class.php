<?php
namespace JRAPP\Pages;

use JRAPP\Posttypes\PageType;

class NotFoundPage extends PageType
{

  public $id;
  public $datos;
  private $slug = 'no-encontrada';
  function __construct()
  {
    $this->id = $this->get_id_by_slug($this->slug);
    $this->obtenerDatos();
  }
  public function obtenerDatos()
  {

    $bannerPrincipal =  [
      'fondo' => get_field('fondo_banner', $this->id),
      'imagen' => get_field('imagen_banner', $this->id),
      'texto' => get_field('texto_banner', $this->id)
    ];


    $this->datos =  [
      'bannerPrincipal'    =>   $bannerPrincipal
    ];
    return $this->datos;
  }
}
