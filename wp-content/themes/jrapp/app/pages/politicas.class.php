<?php
namespace JRAPP\Pages;

class PoliticasPage
{
	public $id = 48;
	public $datos;
	function __construct()
	{
		$this->obtenerDatos();
	}
	public function obtenerDatos()
	{

		$bannerPrincipal =  [
			'fondo' => get_field('fondo_banner', $this->id),
			'imagen' => get_field('imagen_banner', $this->id),
			'texto' => get_field('texto_banner', $this->id)
		];

		$seccPoliticas     = [
			'terminos'     => get_field('terminos_politicas_y_condiciones', $this->id)
		];

		$this->datos =  [
			'bannerPrincipal'  	=> 	$bannerPrincipal,
			'seccPoliticas'     =>  $seccPoliticas
		];
		return $this->datos;
	}
}
