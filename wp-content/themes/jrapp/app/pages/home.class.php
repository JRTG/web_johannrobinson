<?php
namespace JRAPP\Pages;

class HomePage
{
	public $page_id = 5;
	function __construct()
	{
    add_action("wp_ajax_pageHomeAjax", array(&$this, "pageHomeAjax"));
    add_action("wp_ajax_nopriv_pageHomeAjax", array(&$this, "pageHomeAjax"));
  }

  public function pageHomeAjax(){
    $datos = $this->obtenerDatos();
    echo json_encode($datos);
    die();
  }

	public function obtenerDatos()
	{
		$seccMainSlider = 	[
			'slides' => get_field("slides", $this->page_id)
		];
		$wizardSteps = get_field("steps", $this->page_id);

		$datos = [
			'seccMainSlider' 	=> $seccMainSlider,
			'wizardSteps'		=> $wizardSteps
		];


		return $datos;
	}
}
