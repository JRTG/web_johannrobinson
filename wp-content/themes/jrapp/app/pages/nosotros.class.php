<?php
namespace JRAPP\Pages;

use JRAPP\Util\Apariencia;

class NosotrosPage
{
	public $id = 7;
	public $datos;
	function __construct()
	{
		$this->obtenerDatos();
	}
	public function obtenerDatos()
	{

		$bannerPrincipal =  [
			'fondo' => get_field('fondo_banner', $this->id),
			'imagen' => get_field('imagen_banner', $this->id),
			'texto' => get_field('texto_banner', $this->id)
		];

		$seccionHistoria = [
			'hitos' => get_field('hitos', $this->id)
		];

		$seccionProceso = [
			'titulo' => get_field('sp_titulo', $this->id),
			'texto' => get_field('sp_texto', $this->id),
			'procesos' => get_field('sp_lista_procesos', $this->id)
		];

		$seccionProyectos = [
			'titulo' => get_field('spy_titulo', $this->id),
			'texto' => get_field('spy_texto', $this->id)
		];

		$this->datos =  [
			'bannerPrincipal'  	=> 	$bannerPrincipal,
			'seccionHistoria' => $seccionHistoria,
			'seccionProceso' => $seccionProceso,
			'seccionProyectos' => $seccionProyectos
		];
		return $this->datos;
	}
}
