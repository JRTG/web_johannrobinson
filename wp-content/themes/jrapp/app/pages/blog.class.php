<?php
namespace JRAPP\Pages;

class BlogPage
{
	private $page_id = 285;
	function __construct()
	{
		$this->obtenerDatos();
	}
	public function obtenerDatos()
	{
		$bannerPrincipal =  [
			'fondo' => get_field('fondo_banner', $this->page_id),
			'texto' => get_field('texto_banner', $this->page_id),
			'imagen' => get_field('imagen_banner', $this->page_id)
		];
		$datos = [
			'bannerPrincipal'     => $bannerPrincipal
		];


		return $datos;
	}
}
