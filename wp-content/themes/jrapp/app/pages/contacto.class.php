<?php
namespace JRAPP\Pages;

class ContactoPage
{
  public $id = 33;
  public $datos;

  function __construct()
  {
    $this->obtenerDatos();
  }
  public function obtenerDatos()
  {

    $bannerPrincipal =  [
      'fondo' => get_field('fondo_banner', $this->id)['url'],
      'texto' => get_field('texto_banner', $this->id)
    ];
    $popupIngresante  = [
      'inPopActive' => get_field('activar_popup', $this->id),
      'inFoto'     => get_field('foto_postulante', $this->id)['url'],
      'inFotoRostro'     => get_field('foto_rostro', $this->id)['url'],
      'inDatos'     => get_field('datos_postulante', $this->id)
    ];
    $seccContacto     = [
      'telefonos'         => get_field('telefonos_de_contacto', $this->id),
      'correos'           => get_field('correos_de_contacto', $this->id),
      'redesSociales'     => get_field('redes_sociales', $this->id)
    ];
    $seccUbicacion    = [
      'direcs'     => get_field('lista_de_direcciones', $this->id)
    ];

    $this->datos =  [
      'bannerPrincipal'    =>   $bannerPrincipal,
      'popupIngresante'   =>  $popupIngresante,
      'seccContacto'      =>  $seccContacto,
      'seccUbicacion'     =>  $seccUbicacion
    ];
    return $this->datos;
  }
}
