<?php

use JRAPP\Posttypes\ProyectoType;
use JRAPP\Util\Apariencia;

get_header();
$ProyectoType    = new ProyectoType();
$Proyecto = $ProyectoType->obtenerProyecto();
$listaProyectos = $ProyectoType->listarProyectos(['orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => -1], false)['listaProyectos'];
// print('<pre style="font-size:10px;">'.print_r($Contacto->datos,true).'</pre>');
?>
<div id="page" class="proyecto">
  <style>
    .proyecto .ptec-inner:after,
    .proyecto-contenido .g-section.g-section--theme-content.is-alternate {
      background-color: <?= $Proyecto['color'] ?>;
    }

    .g-section .g-section__texto h3,
    .g-section .g-section__texto h2,
    .g-section .g-section__texto a {
      color: <?= $Proyecto['color'] ?>;
    }

    .proyecto .g-title__text__t1,
    .proyecto .g-title__text__t2 {
      color: <?= $Proyecto['color'] ?>;
    }
  </style>
  <div id="proyecto">
    <section class="g-banner-proyecto" style="background-image: url('<?= $Proyecto['banner']['url']; ?>');">
      <div class="gbpy-detail" style="background-color: <?= $Proyecto['color'] ?>;">
        <div class="gbpy-inner g-area-segura">
          <c-title
            pre-title="Proyecto | <?= ($Proyecto['categoria'][0])->name ?>"
            :title='<?= json_encode(Apariencia::formatearTexto($Proyecto['title']))?>'
            :icon = "{src:'<?= $Proyecto['empresa']['logo']['url']; ?>', alt:'<?= $Proyecto['empresa']['logo']['alt']; ?>'}"
            title-tag="h1"
            size="medium"
            :controls="[{
              theme:'circle',
              mod:'primary',
              icon:'icon-share',
              todo:'share',
              title:'compartir'
            }]">
          </c-title>
          <div class="gbpy-imagen">
            <img src="<?= $Proyecto['banner']['url']; ?>" alt="<?= $Proyecto['banner']['alt']; ?>">
          </div>
        </div>
      </div>
    </section>
    <div class="c-breadcrumd">
      <div class="c-breadcrumd__inner g-area-segura">
        <div class="c-breadcrumd__routelist">
          <a href="/" rel="home" class="c-breadcrumd__routelist__item g-ajax-page"><span>Inicio</span></a>
          <a href="/proyectos" rel="proyectos" class="c-breadcrumd__routelist__item"><span>Proyectos</span></a>
          <a href="javascript:void(0)" class="c-breadcrumd__routelist__item"><span><?= $Proyecto['title'] ?></span></a>
        </div>
        <!-- <div class="c-breadcrumd__date"></div> -->
      </div>
    </div>
    <section class="proyecto-contenido">

      <?php for ($i = 0; $i < count($Proyecto['contenido']); $i++) {
        $pc = $Proyecto['contenido'][$i];
      ?>
        <div class="g-section g-section--theme-content <?= $i % 2 == 0 ? '' : 'is-alternate' ?>">
          <div class="g-section__inner has-small-spaces g-area-segura u-flex u-flex-fd-<?= $i % 2 == 0 ? 'r' : 'rr' ?> u-flex-fw-w u-flex-ai-ce u-flex-fd-xs-c">
            <div class="g-section__texto g-section__texto--middle">
              <?= $pc['contenido_texto'] ?>
            </div>
            <div class="g-section__image g-section__image--middle">
              <img class="lozad" data-src="<?= $pc['contenido_imagen']['url'] ?>" alt="<?= $pc['contenido_imagen']['alt'] ?>">
            </div>
          </div>
        </div>
      <?php } ?>
    </section>
    <section class="proyecto-tecnologia g-section">
      <div class="ptec-inner g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <div class="g-title g-title--small">
            <h2 class="g-title__text">
              <div class="g-title__text__t1">
                Las tecnologías de desarrollo
              </div>
              <div class="g-title__text__t2">
                ¡Que tú puedes aprender!
              </div>
            </h2>
            <!-- <div class="g-title__controls">
                            </div> -->
          </div>
        </div>
        <div class="g-area-segura u-flex u-flex-fw-w">
          <div class="g-section__texto g-section__texto--middle">
            <p>Te mostramos las tecnologías que fueron aplicadas a este proyecto porque creemos
              que puedes aprenderlas para colaborar con equipos, proyectos o desarrollas tus
              propias ideas. <a class="g-ajax-page" rel="home" href="/#home-wizard-news">Empieza aquí</a></p>
          </div>
          <div class="g-section__image g-section__image--middle">
            <div class="ptec-lista-tecnologias">
              <?php for ($i = 0; $i < count($Proyecto['prototipo']['tecnologias']); $i++) {
                $tec = $Proyecto['prototipo']['tecnologias'][$i];
              ?>
                <div class="c-tecnologia">
                  <div class="c-tecnologia__imagen">
                    <img class="lozad" data-src="<?= $tec['logo']['url'] ?>" alt="<?= $tec['logo']['alt'] ?>" width="70" height="70">
                  </div>
                  <div class="c-tecnologia__nombre">
                    <?= $tec['nombre'] ?>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="proyecto-lista g-section has-background-color">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <div class="g-title g-title--small">
            <h2 class="g-title__text">
              <div class="g-title__text__t1">
                Tenemos más proyectos
              </div>
              <div class="g-title__text__t2">
                Échales un vistazo
              </div>
            </h2>
            <!-- <div class="g-title__controls">
                  </div> -->
          </div>
        </div>
        <div class="g-area-segura g-area-segura--full-mobile">
          <c-slider ref="sliderPost" theme="theme-02" :conf="sliderConf.sliderT2">
            <c-slide
              v-for='(itemPost, index) in uJsonParse(`<?= json_encode($listaProyectos) ?>`)'
              :key="index">
              <c-proyecto-card :data="itemPost"></c-proyecto-card>
            </c-slide>
          </c-slider>
        </div>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
