<?php
global $contactoPage;
$PC_Datos = $contactoPage->datos;
// print("<pre>".print_r($PC_Datos,true)."</pre>");
if (get_option('Mant') != "SI") {
	header('Location: /');
	exit;
}
?>
<!DOCTYPE html>
<html lang="es-es">

<head>
	<?php if (get_option('Google_Analytics') != '') { ?>
		<!-- Google Analytics -->
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', '<?= get_option('Google_Analytics') ?>', 'auto');
			ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->
	<?php } ?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
	<meta name="theme-color" content="#E6E7E9">
	<meta name="author" content="Johann Robinson">
	<meta name="web_author" content="Johann Robinson">
	<meta name="description" content="<?= get_bloginfo('description'); ?>">
	<meta name="copyright" content="<?= get_bloginfo('name') ?>" />
	<title><?= get_bloginfo('name') ?></title>

	<!-- inicio favicon  iphone retina, ipad, iphone en orden-->
	<link rel="icon" type="image/png" href="<?= MEDIA; ?>/brand/256x256.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= MEDIA; ?>/brand/114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= MEDIA; ?>/brand/72x72.png">
	<link rel="apple-touch-icon-precomposed" href="<?= MEDIA; ?>/brand/57x57.png">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<link rel="apple-touch-startup-image" href="<?= MEDIA; ?>/brand/72x72.png">
	<link rel="apple-touch-icon" href="<?= MEDIA; ?>/brand/72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= MEDIA; ?>/brand/114x114.png">

	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" sizes="256x256" href="<?= MEDIA; ?>/brand/256x256.png">
	<link rel="shortcut icon" sizes="114x114" href="<?= MEDIA; ?>/brand/114x114.png">
	<!-- end favicon -->
	<link rel="manifest" href="/manifest.json">

	<link rel="stylesheet" type="text/css" href="<?= CSS; ?>/styles-small.css?v=<?= VERSION; ?>" />

	<meta property="og:title" content="<?= get_bloginfo('name'); ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?= get_bloginfo('url'); ?>" />
	<meta property="og:description" content="<?= get_bloginfo('description'); ?>" />
	<meta property="og:site_name" content="<?= get_bloginfo('name'); ?>" />
	<meta property="og:image" content="<?= MEDIA; ?>/app/05.jpg" />
	<meta property="og:image:width" content="1136" />
	<meta property="og:image:height" content="640" />

	<script>
		var url_ajax = '<?= admin_url("admin-ajax.php"); ?>';
		var popups = {
			p1: 0
		};
	</script>
</head>

<body class="loading first">
	<div class="cnt-wrapper">
		<div class="wrapper">
      <?php include('components/loader.component.php') ?>
			<div id="page" class="mantenimiento" style="background-image:url(<?= MEDIA . '/app/01.jpg' ?>);">
				<section class="mant">
					<div class="inner-mant g-area-segura">
						<div class="mant-left">
							<div class="mant-cinta t">
								<?= file_get_contents(RUTA_ABSOLUTA . '/assets/media/brand/isotipo.svg'); ?>
							</div>
							<div class="mant-cnt-logo">
								<div class="mant-logo">
									<img src="<?= MEDIA; ?>/brand/logo.svg" alt="Johann Robinson" width="250" height="">
								</div>
								<div class="gi3d-scene3d">
									<div class="gi3d-object3d">
										<div class="gi3d-face3d" id="im28">
											<div class="gi3d-grafico">
												<img src="<?= MEDIA; ?>/brand/isotipob.svg" alt="Johann Robinson">
											</div>
										</div>
										<div class="gi3d-face3d" id="im29">
											<div class="gi3d-grafico">
												<img src="<?= MEDIA; ?>/brand/isotipob.svg" alt="Johann Robinson">
											</div>
										</div>
										<div class="gi3d-face3d" id="im2A">
											<div class="gi3d-grafico">
												<img src="<?= MEDIA; ?>/brand/isotipob.svg" alt="Johann Robinson">
											</div>
										</div>
										<div class="gi3d-face3d" id="im2B">
											<div class="gi3d-grafico">
												<img src="<?= MEDIA; ?>/brand/isotipob.svg" alt="Johann Robinson">
											</div>
										</div>
										<div class="gi3d-face3d" id="im2C">
											<div class="gi3d-grafico">
												<img src="<?= MEDIA; ?>/brand/isotipob.svg" alt="Johann Robinson">
											</div>
										</div>
										<div class="gi3d-face3d" id="im2D">
											<div class="gi3d-grafico">
												<img src="<?= MEDIA; ?>/brand/isotipob.svg" alt="Johann Robinson">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="mant-imagenintro">
								<img src="<?= MEDIA; ?>/app/02.png" alt="" width="350" height="350">
							</div>
							<div class="mant-cinta b">
								<?= file_get_contents(RUTA_ABSOLUTA . '/assets/media/brand/isotipo.svg'); ?>
							</div>
						</div>
						<div class="mant-right">
							<h1 class="mant-title">
								<em>Obtén el respaldo de una</em>
								<strong>Web App en tu negocio</strong>
								<span>¡Muy Pronto Nuestra WebApp!</span>
							</h1>
							<div class="mant-countdown" data-now="<?= (time() * 1000); ?>" data-deploy="<?= get_option('Deploy') ?>">
								<div class="mant-item-cd" id="day">
									<span>DÍAS</span>
									<strong>00</strong>
								</div>
								<div class="mant-item-cd" id="hour">
									<span>HORAS</span>
									<strong>00</strong>
								</div>
								<div class="mant-item-cd" id="minute">
									<span>MINUTOS</span>
									<strong>00</strong>
								</div>
								<div class="mant-item-cd" id="second">
									<span>SEGUNDOS</span>
									<strong>00</strong>
								</div>
							</div>
							<div class="mant-update">
								<a href="" class="g-boton" onclick="window.location.reload()"><em>Actualizar</em></a>
							</div>
							<div class="mant-social">
								<a href="https://web.facebook.com/JRobinsonTeam/" target="_blank" rel="noopener">
									<em>Síguenos en: </em>
									<span class="icon-facebook"></span>
									<em>@JRobinsonTeam</em>
								</a>
							</div>
							<div class="mant-actions">
								<a href="javascript:void(0)" class="g-circle-button g-circle-button--primary icon-notifications menu-helper-item button-notification">Suscríbete</a>
								<a href="" class="g-circle-button g-circle-button--primary icon-share menu-helper-item js-share">Compartir</a>
								<a href="javascript:void(0)" class="g-circle-button g-circle-button--primary icon-downloadapp menu-helper-item button-download">Descarga la aplicación</a>
								<a href="tel:<?= $Contacto->datos['seccContacto']['telefonos']['0']['numero_de_telefono'] ?>" class="g-circle-button g-circle-button--primary icon-phone_in_talk menu-helper-item button-tel">¡Llama ya!</a>

							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="footer launchSetup">
				<section class="footer-copy">
					<div class="footer-cnt g-area-segura">
						<div class="footer-copy-left">
							<p>©<span id="id_year"></span>. Todos los derechos reservados</p>
						</div>
						<div class="footer-copy-right">
							<p>powered by</p>
							<a class="link-jr" target="_blank" href="https://johannrobinson.com/" rel="noopener"><img src="<?= MEDIA; ?>/author/logo-johann.svg" width="80" height="20" alt="">Johann Robinson</a>
						</div>
					</div>
				</section>
			</footer>
		</div>
	</div>

	<div class="g-popup-overlay"></div>
	<?php include('components/setup.component.php') ?>
	<?php include('components/notification.component.php') ?>

	<noscript id="deferred-styles">
		<link href="<?= JS; ?>/app.libraries/slick/slick.css" rel="stylesheet" />
		<link href="<?= JS; ?>/app.libraries/slick/slick-theme.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<?= CSS; ?>/styles.css?v=<?= VERSION; ?>" />
	</noscript>
	<script>
		var loadDeferredStyles = function() {
			var addStylesNode = document.getElementById("deferred-styles");
			var replacement = document.createElement("div");
			replacement.innerHTML = addStylesNode.textContent;
			document.body.appendChild(replacement)
			addStylesNode.parentElement.removeChild(addStylesNode);
		};
		var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
			window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
		if (raf) raf(function() {
			window.setTimeout(loadDeferredStyles, 0);
		});
		else window.addEventListener('load', loadDeferredStyles);
	</script>
	<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
	<script src="/node_modules/vuelidate/dist/vuelidate.min.js"></script>
	<script src="/node_modules/vuelidate/dist/validators.min.js"></script>
	<script src="<?= JS; ?>/app.libraries/jquery/jquery-2.0.min.js" type="text/javascript"></script>
	<script src="<?= JS; ?>/app.libraries/slick/slick.min.js"></script>
	<?php if (get_option('Maps') != '') { ?>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=<?= get_option('Maps') ?>"></script>
		<script>
			var ubigeo = {
				mic: "<?= MEDIA; ?>/app/map-elite.png",
				lat: <?= $PC_Datos['seccUbicacion']['direcs']['0']['direccion']['lat']; ?>,
				lng: <?= $PC_Datos['seccUbicacion']['direcs']['0']['direccion']['lng']; ?>
			}
		</script>
	<?php } ?>
	<script src="<?= JS; ?>/app.main.js?v=<?= VERSION; ?>" type="text/javascript"></script>
</body>

</html>
