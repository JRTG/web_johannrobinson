<?php

use JRAPP\Pages\NotFoundPage;

get_header();
$page404 = (new NotFoundPage)->datos;
?>
<div id="page" class="404">
  <section class="g-section has-background has-background--is-white is-full-screen u-flex u-flex-ai-ce" style="background-image: url('<?= $page404["bannerPrincipal"]["fondo"]['url'] ?>');">
    <div class="g-section__inner has-large-spaces g-area-segura u-flex u-flex-fd-r u-flex-fw-w u-flex-ai-ce u-flex-fd-xs-c">
      <div class="g-section__texto g-section__texto--middle">
        <div class="g-title g-title--large">
            <div class="g-title__text">
                <?php $texto = explode('|', $page404["bannerPrincipal"]["texto"]) ?>
                <div class="g-title__text__t1">
                  <?= $texto[0] ?>
                </div>
                <h1 class="g-title__text__t2">
                  <?= $texto[1] ?>
                </h1>
                <p>
                  <?= $texto[2]?>
                </p>
            </div>
            <div class="g-title__controls">
              <a href="/" class="g-boton g-ajax-page" rel="home">
                <em>Ir al inicio</em>
              </a>
              <a href="/" class="g-circle-button g-circle-button--primary icon-share js-share">
                <em>Compartir</em>
              </a>
            </div>
        </div>
      </div>
      <div class="g-section__image g-section__image--middle u-tx-al-ce">
        <img src="<?= $page404["bannerPrincipal"]['imagen']['url'] ?>" alt="<?= $page404["bannerPrincipal"]['imagen']['alt'] ?>">
      </div>
    </div>
  </section>
</div>
<?php get_footer(); ?>
