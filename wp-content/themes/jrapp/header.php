<?php
use JRAPP\Util\Apariencia;
global $contactoPage;
global $CurrentUser;
if (!current_user_can('edit_pages') || !is_user_logged_in()) {
  if (get_option('Mant') == "SI") {
    header('Location: /mantenimiento');
    exit;
  }
}

?>
<!DOCTYPE html>
<html lang="es-es">

<head>
  <?php if (get_option('Google_Analytics') != '') { ?>
    <!-- Google Analytics -->
    <script>
      (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

      ga('create', '<?= get_option('Google_Analytics') ?>', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
  <?php
  } ?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">

  <meta name="theme-color" content="#E6E7E9">
  <meta name="author" content="Johann Robinson">
  <meta name="web_author" content="Johann Robinson">
  <meta name="description" content="<?= get_the_excerpt() ? get_the_excerpt() : get_bloginfo('description'); ?>">
  <meta name="copyright" content="<?= get_bloginfo('name') ?>" />
  <title id="page-title"><?= get_the_title() . ' - ' . get_bloginfo('name') ?></title>

  <!-- inicio favicon  iphone retina, ipad, iphone en orden-->
  <link rel="icon" type="image/png" href="<?= MEDIA; ?>/brand/256x256.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= MEDIA; ?>/brand/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= MEDIA; ?>/brand/72x72.png">
  <link rel="apple-touch-icon-precomposed" href="<?= MEDIA; ?>/brand/57x57.png">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <link rel="apple-touch-startup-image" href="<?= MEDIA; ?>/brand/72x72.png">
  <link rel="apple-touch-icon" href="<?= MEDIA; ?>/brand/72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= MEDIA; ?>/brand/114x114.png">

  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="256x256" href="<?= MEDIA; ?>/brand/256x256.png">
  <link rel="shortcut icon" sizes="114x114" href="<?= MEDIA; ?>/brand/114x114.png">
  <!-- end favicon -->
  <link rel="manifest" href="/manifest.json">

  <link rel="stylesheet" type="text/css" href="<?= CSS; ?>/styles-small.css?v=<?= VERSION; ?>" />

  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/icomoon.ttf?1ml1s2" as="font" type="font/ttf" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/icomoon.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Raleway-Regular.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Raleway-SemiBold.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Raleway-Light.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Raleway-Medium.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Raleway-Bold.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Gilroy-Bold.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Gilroy-Medium.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Gilroy-Regular.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="<?= TEMPPATH ?>/assets/fonts/Gilroy-Light.woff" as="font" type="font/woff" crossorigin>


  <meta property="fb:app_id" content="146569586036682" />
  <meta property="og:title" content="<?= get_the_title() . ' - ' . get_bloginfo('name'); ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?= get_the_permalink() ? get_the_permalink() : get_bloginfo('url'); ?>" />
  <meta property="og:description" content="<?= get_the_excerpt() ? get_the_excerpt() : get_bloginfo('description'); ?>" />
  <meta property="og:site_name" content="<?= get_bloginfo('name'); ?>" />
  <meta property="og:image" content="<?= get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : (MEDIA . '/app/05.jpg'); ?>" />
  <meta property="og:image:width" content="1200" />
  <meta property="og:image:height" content="627" />

  <script>
    var url_ajax = '<?= admin_url("admin-ajax.php"); ?>';
  </script>
</head>



<body class="loading first st">
  <!-- html solo para el menu responsive -->
  <div class="menu-mobile-open g-circle-button g-circle-button--secondary icon-menu"></div>
  <div class="menu-mobile-close g-circle-button g-circle-button--primary icon-close"></div>
  <div class="menu-touchslide menuts touch"></div>
  <div class="menu-overlay menuts touch"></div>
  <!-- html solo para el menu responsive -->

  <div class="cnt-wrapper">
    <!-- HEADER START -->
    <?php include('components/loader.component.php') ?>
    <header id="header" class="c-header" v-bind:class="{'is-fixed': header.states.isFixed , 'is-scrolling': header.states.isScroll}" data-mant="<?= get_option('Mant') ?>">
      <div class="g-section">
        <div class="c-header__content g-area-segura u-flex u-flex-ai-ce u-flex-jc-sb">
          <a href="/" rel="home" class="c-header__logo g-ajax-page">
            <img src="<?= MEDIA; ?>/brand/logo.svg" alt="<?= get_bloginfo('name') ?>" alt="Johann Robinson" width="200" height="108">
          </a>
          <nav class="menu u-flex u-flex-ai-ce">
            <div class="menu-helper">
              <c-button theme="circle" :mod2="'menu-helper-item '+ (buttons.notification?'u-show-inline':'u-hide')" icon="icon-notifications" title="suscríbete" v-on:click-button="this.JRAPP.modules.popups.activePopup = 'notification'"></c-button>
              <c-button theme="circle" :mod2="'menu-helper-item '+ (buttons.install?'u-show-inline':'u-hide')" icon="icon-downloadapp" title="Descarga la app" v-on:click-button="this.JRAPP.modules.popups.activePopup = 'setup'"></c-button>
              <c-button url="tel:<?= $contactoPage->datos['seccContacto']['telefonos']['0']['numero_de_telefono'] ?>" theme="circle" mod2="menu-helper-item u-hide u-show-sm-inline" icon="icon-phone_in_talk" title="¡Llama ya!"></c-button>
            </div>
            <div class="menu-wrap u-flex u-flex-ai-ce">
              <?php Apariencia::construirMenu(1); ?>

            </div>

          </nav>
        </div>
      </div>
    </header>
    <div class="wrapper">
