<?php

use JRAPP\Util\Apariencia;

global $contactoPage;
$PC_Datos = $contactoPage->datos;
// print("<pre>".print_r($popIn,true)."</pre>");
?>
</div>
<footer class="footer launchSetup">

  <!-- creditos -->
  <div class="footer-bottom">
    <div class="footer-cnt g-area-segura">
      <div class="footer-cnt-logo">
        <div class="footer-logo">
          <img src="<?= MEDIA; ?>/brand/logo-blanco.svg" alt="Johann Robinson">
        </div>
      </div>
      <div class="footer-cnt-item">
        <div class="footer-item i1">
          <div class="footer-item-title">
            <h3>Ubícanos</h3>
          </div>
          <div class="footer-item-detail">
            <?php
            $count     = 0;
            $size    = count($PC_Datos['seccUbicacion']['direcs']);
            foreach ($PC_Datos['seccUbicacion']['direcs'] as $dir) {
              $count++;
            ?>
              <p><?= $dir['direccion']['address'] ?><?= ($count == $size) ? '' : '<span>/</span>'; ?></p>
            <?php
            } ?>
          </div>
        </div>
        <div class="footer-item i2">
          <div class="footer-item-title">
            <h3>Central Telefónica</h3>
          </div>
          <div class="footer-item-detail">
            <?php
            $count     = 0;
            $size    = count($PC_Datos['seccContacto']['telefonos']);
            foreach ($PC_Datos['seccContacto']['telefonos'] as $tel) {
              $count++;
            ?>
              <a href="tel:<?= $tel['numero_de_telefono']; ?>"><?= $tel['numero_de_telefono']; ?></a><?= ($count == $size) ? '' : '<span>/</span>'; ?>
            <?php
            } ?>
          </div>
        </div>
        <div class="footer-item i3">
          <div class="footer-item-title">
            <h3>Correo Electrónico</h3>
          </div>
          <div class="footer-item-detail">

            <a href="mailto:<?= $PC_Datos['seccContacto']['correos']['0']['correo_electronico'] ?>"><?= $PC_Datos['seccContacto']['correos']['0']['correo_electronico'] ?></a>
          </div>
        </div>
        <div class="footer-item i4">
          <div class="footer-item-title">
            <h3>Síguenos en:</h3>
          </div>
          <div class="footer-item-detail">
            <div class="footer-redes">
              <div class="footer-redes__text">
                <em>Síguenos en:</em>
              </div>
              <ul class="footer-redes__list">
                <?php foreach ($PC_Datos['seccContacto']['redesSociales'] as $red) { ?>
                  <li><a href="<?= $red['link']; ?>" class="icon-<?= $red['nombre_de_red']; ?>" target="blank" rel="noopener"><?= $red['nombre_de_red']; ?></a></li>
                <?php
                } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="footer-copy">
    <div class="footer-cnt g-area-segura">
      <div class="footer-copy-left">
        <p><?= bloginfo('name') ?> ©<span id="id_year"></span> Todos los derechos reservados</p>
      </div>
      <div class="footer-copy-right">
        <p>powered by</p>
        <a class="link-jr" target="_blank" href="https://johannrobinson.com/" rel="noopener"><img src="<?= MEDIA; ?>/author/logo-johann.svg" width="80" height="20" alt="Johann Robinson">Johann Robinson</a>
      </div>
    </div>
  </section>
</footer>
</div>


<!-- contenedor del menu responsive -->
<div class="menu-sidebar menuts touch" id="menu-mobile">
  <div class="menu-sidebar-cnt">
    <a v-on:click.prevent="this.JRAPP.components.page.goTo('/','home')" class="menu-logo" v-if="!auth.currentUser">
      <img src="<?= MEDIA; ?>/brand/logo.svg" alt="<?= get_bloginfo('name') ?>" alt="Johann Robinson" width="200" height="108">
    </a>

    <?php Apariencia::construirMenu(1); ?>
  </div>
</div>

<div id="popups">
  <div class="g-popup-overlay" v-bind:class="this.activePopup&&'active'" v-on:click="closeAll"></div>
  <?php include('components/setup.component.php') ?>
  <?php include('components/notification.component.php') ?>
  <?php include('components/sesion.component.php') ?>
</div>

<!-- [CSS Optimizado] -->
<noscript id="deferred-styles">
  <link href="<?= JS; ?>/app.libraries/slick/slick.css" rel="stylesheet" />
  <link href="<?= JS; ?>/app.libraries/slick/slick-theme.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?= CSS; ?>/styles.css?v=<?= VERSION; ?>" />
</noscript>
<script>
  var loadDeferredStyles = function() {
    var addStylesNode = document.getElementById("deferred-styles");
    var replacement = document.createElement("div");
    replacement.innerHTML = addStylesNode.textContent;
    document.body.appendChild(replacement)
    addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  if (raf) raf(function() {
    window.setTimeout(loadDeferredStyles, 0);
  });
  else window.addEventListener('load', loadDeferredStyles);
</script>
<?php if(WP_DEBUG) {?>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.6.0/dist/vue.js"></script>
<?php } else { ?>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<?php } ?>

<script src="/node_modules/vuelidate/dist/vuelidate.min.js"></script>
<script src="/node_modules/vuelidate/dist/validators.min.js"></script>
<script src="<?= JS; ?>/app.libraries/jquery/jquery-2.0.min.js" type="text/javascript"></script>
<script src="<?= JS; ?>/app.libraries/slick/slick.min.js"></script>
<!-- <script src="<?= JS; ?>/app.libraries/deltasc/deltasc.min.js?v=1"></script> -->
<?php if (get_option('Maps') != '') { ?>
  <script rel="preconnect" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=<?= get_option('Maps') ?>"></script>
  <script>
    var ubigeo = {
      mic: "<?= MEDIA; ?>/brand/marker.png",
      lat: <?= $PC_Datos['seccUbicacion']['direcs']['0']['direccion']['lat']; ?>,
      lng: <?= $PC_Datos['seccUbicacion']['direcs']['0']['direccion']['lng']; ?>
    }
  </script>
<?php
} ?>
<script src="<?= JS; ?>/app.main.js?v=<?= VERSION; ?>" type="text/javascript"></script>
<div class="htcc-messenger">
  <div class="fb-customerchat" page_id="196982184369605" theme_color="#000000" logged_in_greeting="Hola, te damos la bienvenida" logged_out_greeting="Hola, te damos la bienvenida" ref="" minimized="">
  </div>
</div>
</body>

</html>
