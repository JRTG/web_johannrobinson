<?php

use JRAPP\Pages\BlogPage;
use JRAPP\Pages\HomePage;
use JRAPP\Pages\ProyectoPage;
use JRAPP\Posttypes\PerfilType;
use JRAPP\Posttypes\PostType;
use JRAPP\Posttypes\ProyectoType;

get_header();

$homePage 	= new HomePage();
$blogPage	= new BlogPage();
$proyectoPage	= new ProyectoPage();

$postType = new PostType();
$proyectoType = new ProyectoType();
$perfilType = new PerfilType();

$listaPosts = $postType->listarPosts(['orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => 8], false)['listaPosts'];
$listaProyectos = $proyectoType->listarProyectos(['orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => 8], false)['listaProyectos'];
$listaPerfiles = ($perfilType->listarPerfiles(['orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => -1], false))['listaPerfiles'];

$pageHome	= $homePage->obtenerDatos();
$pageProyecto = $proyectoPage->obtenerDatos();
$pageBlog = $blogPage->obtenerDatos();

// print("<pre>".print_r($listaIntenciones,true)."</pre>");
// print("<pre>".print_r($PAGE_NOSOTROS->datos['seccIdentidad']['años'],true)."</pre>");
// print("<pre>".print_r($LST_NIVELES,true)."</pre>");
// print("<pre>".print_r($LST_TALLERES,true)."</pre>");
//print("<pre>".print_r($PAGE_CONTACTO->datos['popupIngresante'],true)."</pre>");
?>

<div id="page" class="home">
  <h1 style="display:none;">Johann Robinson Development Team</h1>
  <div id="home">
    <section class="home-slider g-section">
      <c-slider :conf="sliderConf.sliderT0" mod="home-slider-t0">
        <c-slide
          mod="hs-item-t0"
          v-for='(slide, index) in uJsonParse(`<?= json_encode($pageHome['seccMainSlider']['slides']) ?>`)'
          :key="index">
          <c-banner-page
            :fondo="slide.fondo.url"
            :image="{src: slide.foto.url, alt: slide.foto.alt}"
            :title="slide.titulo.split('|')"
            title-mod-text="u-tx-ov-el u-ws-nw"
            :title-controls="[{
                theme: 'regular',
                url: slide.link['link_url'].url,
                todo: slide.link['link_tipo'],
                title: slide.link['link_url'].title,
                target: slide.link['link_url'].target,
                rel: slide.link['link_rel']
              },
              {
                theme:'circle',
                mod:'primary',
                icon:'icon-share',
                todo:'share',
                url:slide.link['link_url'].url,
                title:'compartir'
              }]"
          ></c-banner-page>
        </c-slide>
      </c-slider>

    </section>
    <section class="home-wizard-news" id="home-wizard-news">
      <div class="hwn-inner hwn-top g-area-segura">
        <h2 class="g-title-section">
          <span class="gts-icon icon-jr"></span>
          <div class="gts-text">
            <em>{{ wizardUI.stepActive==0?'Cuéntanos quién eres':(wizardUI.stepActive==1?'Genial, ¿Cuál es tu nombre?':(wizardUI.stepActive==2?'Bienvenido '+cperfil[1]:(wizardUI.stepActive==3?'Eres un '+cperfil[1]:(wizardUI.stepActive==4?'Hemos finalizado':''))))}}</em>
            <div class="g-subtitle-section">{{ wizardUI.stepActive==0?'Elige un perfil para ti':(wizardUI.stepActive==1? cperfil[1] :(wizardUI.stepActive==2?this.formUserData.nombre:(wizardUI.stepActive==3?this.formUserData.nombre+', ¿Cuál es tu idea?':(wizardUI.stepActive==4?this.formUserData.nombre:''))))}}</div>
          </div>
        </h2>
      </div>
      <div class="hwn-inner hwn-bottom g-area-segura g-area-segura--full-mobile u-flex-fd-xs-c">
        <div class="home-wizard">
          <div class="hw-content-steps">
            <div class="hw-step sl" v-bind:class="{active: wizardUI.stepActive==0}">
              <div class="hw-step-top">
                <div class="g-section__texto"><?= $pageHome['wizardSteps'][0]['step_text'] ?></div>
              </div>
              <c-slider :conf="sliderConf.sliderT1" theme="theme-01">
                <c-slide
                  v-for='(itemPerfil, index) in uJsonParse(`<?= json_encode($listaPerfiles) ?>`)'
                  :key="index">
                  <c-elipse-card
                    v-model="formProfileData.perfil"
                    name="perfil"
                    is-input="true"
                    :data='itemPerfil'
                    v-on:change-option="obtenerIntenciones()"
                  ></c-elipse-card>
                </c-slide>
              </c-slider>
            </div>

            <div class="hw-step " v-bind:class="{active: wizardUI.stepActive==1}">
              <div class="hw-step-top">
                <div class="g-section__texto"><?= $pageHome['wizardSteps'][1]['step_text'] ?></div>
              </div>
              <div class="hws-step-inner hws-step-inner--mw">
                <div class="hw-step-left">
                  <c-elipse-card
                    v-if="cperfil"
                    is-active="true"
                    :data="{
                            imagen:{
                              url:cperfil[2],
                              alt:'Perfil Seleccionado'
                            },
                            title:cperfil[1]
                          }"
                  ></c-elipse-card>
                  <a href="#" v-on:click.prevent="cambiarPerfil()" class="c-link">Cambiar perfil</a>
                </div>
                <div class="hw-step-right">
                  <form class="hws-form-user">
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
                      <label for="wi-nombre" class="c-input" v-bind:class="{active: $v.formUserData.nombre.$dirty, error: $v.formUserData.nombre.$error}">
                        <div class="c-label" v-if="$v.formUserData.nombre.$dirty && !$v.formUserData.nombre.$error">
                          <em>Ingresar nombre</em>
                        </div>
                        <div class="c-label error" v-if="$v.formUserData.nombre.$error">
                          <em v-if="!$v.formUserData.nombre.required">*Nombre requerido</em>
                          <em v-if="!$v.formUserData.nombre.alpha">*Sólo letras</em>
                        </div>
                        <input id="wi-nombre" type="text" v-model="formUserData.nombre" :disabled="wizardUI.loading" placeholder="Dinos tu nombre" @focusout="$v.formUserData.nombre.$touch()" @input="$v.formUserData.nombre.$touch()">
                      </label>
                    </div>
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
                      <label for="wi-tel" class="c-input" v-bind:class="{active: $v.formUserData.tel.$dirty, error: $v.formUserData.tel.$error}">
                        <div class="c-label" v-if="$v.formUserData.tel.$dirty && !$v.formUserData.tel.$error">
                          <em>Ingresar teléfono</em>
                        </div>
                        <div class="c-label error" v-if="$v.formUserData.tel.$error">
                          <em v-if="!$v.formUserData.tel.required">*Teléfono requerido</em>
                          <em v-if="!$v.formUserData.tel.numeric">*Sólo números</em>
                          <em v-if="!$v.formUserData.tel.minLength">*Mínimo 7 números</em>
                          <em v-if="!$v.formUserData.tel.maxLength">*Máximo 9 números</em>
                        </div>
                        <input id="wi-tel" type="tel" v-model="formUserData.tel" :disabled="wizardUI.loading" placeholder="Dinos tu teléfono" @focusout="$v.formUserData.tel.$touch()" @input="$v.formUserData.tel.$touch()">
                      </label>
                    </div>
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
                      <label for="wi-email" class="c-input" v-bind:class="{active: $v.formUserData.email.$dirty, error: $v.formUserData.email.$error}">
                        <div class="c-label" v-if="$v.formUserData.email.$dirty && !$v.formUserData.email.$error">
                          <em>Ingresar email</em>
                        </div>
                        <div class="c-label error" v-if="$v.formUserData.email.$error">
                          <em v-if="!$v.formUserData.email.email">*Email no válido</em>
                          <em v-if="!$v.formUserData.email.required">*Email requerido</em>
                        </div>
                        <input id="wi-email" type="text" v-model="formUserData.email" :disabled="wizardUI.loading" placeholder="Ingresa tu email" @focusout="$v.formUserData.email.$touch()" @input="$v.formUserData.email.$touch()">
                      </label>
                    </div>
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c" v-if="!wizardUI.notified">
                      <label for="wi-notif" class="g-checkbox">
                        <input id="wi-notif" type="checkbox" v-model="formUserData.notif">
                        <span></span>
                        <em>Deseo estar enterado y recibir notificaciones</em>
                        <!-- <pre style="font-size: 10px;">{{formUserData.notif}}</pre> -->
                      </label>
                    </div>
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
                      <div class="c-input">
                        <button v-on:click.prevent="this.setTimeout(()=>{goToStep(2,$v.formUserData.$invalid)},550)" :disabled="$v.formUserData.$invalid || wizardUI.loading" class="g-boton">
                          <em>Siguiente</em>
                        </button>
                      </div>
                      <a href="#" v-on:click.prevent="cambiarPerfil()" class="c-link u-hide u-show-xs">Cambiar perfil</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="hw-step sl" v-bind:class="{active: wizardUI.stepActive==2}">
              <div class="hw-step-top" v-if="listaIntenciones.length>0">
                <div class="g-section__texto"><?= $pageHome['wizardSteps'][2]['step_text'] ?></div>
                <!-- <pre style="font-size: 10px;">{{$v.formProfileData.$invalid}}</pre> -->
              </div>
              <c-slider ref="sliderIntenciones" :conf="sliderConf.sliderT1" theme="theme-01" v-if="listaIntenciones.length>0">
                <c-slide
                  v-for="(itemIntencion, index) in listaIntenciones"
                  :key="index">
                  <c-elipse-card
                    v-model="formIntentData.intencion"
                    name="intencion"
                    is-input="true"
                    :data='itemIntencion'
                    v-on:change-option="this.setTimeout(()=>{goToStep(3,$v.formIntentData.$invalid)},550)"
                  ></c-elipse-card>
                </c-slide>
              </c-slider>

              <div class="hw-step-inner" v-else>
                <div class="hw-step-img"><img src="<?= $pageHome['wizardSteps'][2]['step_image']['url'] ?>" alt="<?= $pageHome['wizardSteps'][2]['step_image']['alt'] ?>"></div>
                <div class="hw-step-top">
                  <div class="g-section__texto u-tx-al-ce">
                    <p>Estamos trabajando intenciones nuevas para este perfil o puede que hayas perdido la conexión a internet intenta más tarde. <a href="" class="c-link" v-on:click.prevent="goToStep(0,false)">Volver a ver los perfiles</a></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="hw-step" v-bind:class="{active: wizardUI.stepActive==3}">
              <div class="hw-step-top">
                <div class="g-section__texto"><?= $pageHome['wizardSteps'][3]['step_text'] ?></div>
              </div>
              <div class="hws-step-inner hws-step-inner--mw">
                <div class="hw-step-left">
                <c-elipse-card
                    v-if="cintencion"
                    is-active="true"
                    :data="cintencion"
                  ></c-elipse-card>
                  <a href="#" v-on:click.prevent="cambiarIntencion()" class="c-link">Cambiar intención</a>
                </div>
                <div class="hw-step-right">
                  <form class="hws-form-user">
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
                      <label for="wi-idea" class="c-textarea" v-bind:class="{active: $v.formIdeaData.idea.$dirty, error: $v.formIdeaData.idea.$error,disabled:wizardUI.loading}">
                        <div class="c-label" v-if="$v.formIdeaData.idea.$dirty && !$v.formIdeaData.idea.$error">
                          <em>Tu idea aquí</em>
                        </div>
                        <div class="c-label error" v-if="$v.formIdeaData.idea.$error">
                          <em v-if="!$v.formIdeaData.idea.required">*Idea requerida</em>
                        </div>
                        <textarea id="wi-idea" v-model="formIdeaData.idea" :disabled="wizardUI.loading" placeholder="Tu idea aquí" @focusout="$v.formIdeaData.idea.$touch()" @input="$v.formIdeaData.idea.$touch()"></textarea>
                      </label>
                    </div>
                    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
                      <div class="c-input">
                        <button v-on:click.prevent="registrarIdea()" :disabled="$v.formIdeaData.$invalid" class="g-boton" v-bind:class="{'is-loading': wizardUI.loading}">
                          <em>Siguiente</em>
                        </button>
                      </div>
                      <a href="#" v-on:click.prevent="cambiarIntencion()" class="c-link u-hide u-show-xs">Cambiar intención</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="hw-step" v-bind:class="{active: wizardUI.stepActive==4}">
              <!--  -->
              <div class="hws-step-inner" v-if="cperfil != null && cintencion != null">
                <div class="hw-step-img"><img src="<?= $pageHome['wizardSteps'][4]['step_image']['url'] ?>" alt="<?= $pageHome['wizardSteps'][4]['step_image']['alt'] ?>"></div>
                <div class="g-section__texto u-tx-al-ce"><?= $pageHome['wizardSteps'][4]['step_text'] ?></div>
              </div>
            </div>
          </div>
        </div>
        <div class=" home-news">
          <div class="hn-texto">
            <em>Tus clientes están en internet ahora,</em>
            <strong>¡Tu negocio también!</strong>
          </div>
          <div class="hn-controls">
            <c-button
              theme="regular"
              url="/nosotros"
              todo="internal"
              title="conócenos"
              rel="nosotros"></c-button>
          </div>
        </div>
      </div>
    </section>

    <section class="g-section g-section--theme-landing" id="proyectos">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <c-title
            :title="['Estamos felices de poder ayudar','¡Da el salto digital!']"
            size="small"
          ></c-title>
          <div class="g-section__texto g-section__texto--middle">
            <p>Hemos comenzado a darle Progresives Web App a empresas de provincia. Te mostramos unos proyectos.</p>
          </div>
        </div>
        <div class="g-area-segura g-area-segura--full-mobile">
          <c-slider theme="theme-02" :conf="sliderConf.sliderT2">
            <c-slide
              v-for='(itemProyecto, index) in uJsonParse(`<?= json_encode($listaProyectos) ?>`)'
              :key="index">
              <c-proyecto-card :data="itemProyecto"></c-proyecto-card>
            </c-slide>
          </c-slider>
        </div>
        <div class="g-section__image">
          <img class="lozad" data-src="<?= $pageProyecto['bannerPrincipal']['imagen']['url'] ?>" alt="<?= $pageProyecto['bannerPrincipal']['imagen']['url'] ?>" width="450" height="450">
        </div>
      </div>
    </section>
    <section class="g-section g-section--theme-landing has-background" id="blog" style="background-image: url(<?= $pageBlog['bannerPrincipal']['fondo']['url'] ?>);">
      <div class="g-section__inner has-medium-spaces">
        <div class="g-area-segura">
          <c-title
            :title="['Curiosidades digitales en','¡Nuestro Blog!']"
            size="small"
          ></c-title>
          <div class="g-section__texto g-section__texto--middle">
            <p>Te mostramos las novedades e información valiosa de la tecnología que mueve al mundo.</p>
          </div>
        </div>
        <div class="g-area-segura g-area-segura--full-mobile">
          <c-slider ref="sliderPost" theme="theme-02" :conf="sliderConf.sliderT2">
            <c-slide
              v-for='(itemPost, index) in uJsonParse(`<?= json_encode($listaPosts) ?>`)'
              :key="index">
              <c-post-card :data="itemPost"></c-post-card>
            </c-slide>
          </c-slider>
        </div>
        <div class="g-section__image">
          <img class="lozad" data-src="<?= $pageBlog['bannerPrincipal']['imagen']['url'] ?>" alt="<?= $pageBlog['bannerPrincipal']['imagen']['alt'] ?>" width="450" height="450">
        </div>
      </div>
    </section>
  </div>
</div>
<?php get_footer(); ?>
