JRAPP.modules.header = new Vue({
  el: '#header',
  name: "header",
  mixins: [JRAPP.services.user],
  data() {
    return {
      header: {
        menuContextActive: null,
        states: {
          isScroll: false,
          isFixed: true
        },
        dim: {
          altoScroll: 0,
          position: $(window).scrollTop()
        }
      },
      buttons: {
        install: false,
        notification: false
      },
      auth: {
        currentUser: JSON.parse(localStorage.getItem('currentUser'))
      }
    }
  },
  methods: {
    headerPosition: function (event) {
      this.header.dim.altoScroll = $(window).scrollTop();
      if(this.header.menuContextActive) this.header.menuContextActive = null;
      if (this.header.dim.altoScroll > 0) {
        this.header.states.isScroll = true;
      } else {
        this.header.states.isScroll = false;
      }

      if (this.header.dim.altoScroll > this.header.dim.position) {
        //solución al bouncing de Iphone X
        if (this.header.dim.position > 80) {
          this.header.states.isFixed = false;
          $('.menu-mobile-open').addClass('is-scrolling');
        } else {
          this.header.states.isFixed = true;
          $('.menu-mobile-open').removeClass('is-scrolling');
        }
        //end
      } else {
        this.header.states.isFixed = true;
        $('.menu-mobile-open').removeClass('is-scrolling');
      }

      this.header.dim.position = this.header.dim.altoScroll;
    },
    openMenuContext: function (menu) {
      this.header.menuContextActive = menu;
      window.addEventListener("click", this.closesAllMenuContext, { passive: true });
      window.addEventListener("resize", this.closesAllMenuContext, { passive: true });
    },
    closesAllMenuContext: function (event) {
      this.header.menuContextActive = null;
      window.removeEventListener("click", this.closesAllMenuContext, { passive: true });
      window.removeEventListener("resize", this.closesAllMenuContext, { passive: true });
    },
    logout: function (event) {
      this.authLogout()
        .then((data) => {
          this.auth.currentUser = null;
          localStorage.removeItem('currentUser');
          JRAPP.components.page.goTo('/', 'home');
        })
    },
    isLoggedIn: function (event) {
      this.auth.currentUser = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')) : null;
    }
  },
  created() {
    window.addEventListener("scroll", this.headerPosition);
  },
  destroyed() {
    window.removeEventListener("scroll");
  },
  mounted() {
    // $('.menu-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('menu-list');
    // $('.menu-responsive').removeAttr('id');
    // $('.menu-responsive').find('.menu-item').removeClass('menu-item').addClass('menu-item-mobile').removeAttr('id');
    // $('.menu-responsive').find('.menu-list').removeAttr('id');
    // $('.c-header__logo').clone().prependTo('.menu-sidebar-cnt').removeClass('c-header__logo').addClass('menu-logo');
    this.headerPosition();
  }
})
