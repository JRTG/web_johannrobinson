JRAPP.modules.popups = new Vue({
  el: '#popups',
  name: "popups",
  data() {
    return {
      activePopup: null
    }
  },
  methods: {
    closeAll() {
      this.activePopup = null;
    }
  }
})
