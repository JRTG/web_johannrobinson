JRAPP.modules['menu-mobile'] = new Vue({
  el: '#menu-mobile',
  name: "menu-mobile",
  mixins: [JRAPP.services.user],
  data() {
    return {
      auth: {
        currentUser: JSON.parse(localStorage.getItem('currentUser'))
      }
    }
  },
  methods: {
    logout: function (event) {
      JRAPP.components.menu.close();
      this.auth.currentUser = null;
      JRAPP.components.menu.close();
      localStorage.removeItem('currentUser');
      JRAPP.components.page.goTo('/', 'home');
      this.authLogout()
        .then((data) => {
          console.log(data);
        })
    },
    isLoggedIn: function (event) {
      this.auth.currentUser = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')) : null;
    }
  }
})
