JRAPP.pages.home = new JRAPP.utils.page("home"),
  JRAPP.pages.home.socket = function () {
    var host = 'ws://localhost:12345/wp-content/themes/jrapp/controllers/chat.class.php';
    var socket = new WebSocket(host);
    socket.onmessage = function (e) {
      document.getElementById('SocketMessage').innerHTML = e.data;
    };
  },
  JRAPP.pages.home.module = { options: null, instance: null },
  JRAPP.pages.home.module.options = {
    el: '#home',
    name: 'home',
    mixins: [vuelidate.validationMixin],
    validations: {
      formProfileData: {
        perfil: {
          required: validators.required
        }
      },
      formUserData: {
        nombre: {
          required: validators.required,
          alpha: (nombre) => { return /^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñ\s]*$/.test(nombre) }
        },
        email: {
          required: validators.required,
          email: validators.email
        },
        tel: {
          required: validators.required,
          numeric: validators.numeric,
          minLength: validators.minLength(7),
          maxLength: validators.maxLength(9)
        }
      },
      formIntentData: {
        intencion: {
          required: validators.required
        }
      },
      formIdeaData: {
        idea: {
          required: validators.required
        }
      }
    },
    data() {
      return {
        wizardUI: {
          stepActive: 0,
          loading: false,
          notified: true
        },
        pageData: null,
        listaIntenciones: [],
        listaPerfiles: [],
        listaProyectos: [],
        listaPosts: [],
        formProfileData: {
          perfil: false
        },
        formUserData: {
          nombre: "",
          email: "",
          tel: "",
          notif: false
        },
        formIntentData: {
          intencion: false
        },
        formIdeaData: {
          idea: ""
        },
        sliderConf: {
          sliderT0: {
            infinite: true,
            arrows: false,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            // speed: 1000,
            fade: true,
            cssEase: 'linear'
          },
          sliderT1: {
            infinite: false,
            arrows: false,
            dots: true,
            customPaging: function (slider, i) {
              return `<div class="c-slider__dot">${i}</div>`;
            },
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: 1199,
                settings: {
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 575,
                settings: {
                  slidesToShow: 1
                }
              }
            ]
          },
          sliderT2: {
            infinite: false,
            arrows: true,
            dots: true,
            customPaging: function (slider, i) {
              return `<div class="c-slider__dot">${i}</div>`;
            },
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          }
        }
      }
    },
    computed: {
      cperfil: function () {
        if (this.formProfileData.perfil) {
          return this.formProfileData.perfil.split('|');
        } else {
          return false;
        }
      },
      cintencion: function () {
        if (this.formIntentData.intencion) {
          let index = this.formIntentData.intencion.split('|')[0];
          let intencion = this.listaIntenciones.filter(function (inten) {
            return inten.id == index;
          })[0];
          return intencion;
        } else {
          return false;
        }
      }
    },
    updated: function () {
      this.$nextTick(function () {
        // Código que se ejecutará solo después
        // de que se haya vuelto a renderizar toda la vista

        if (this.listaIntenciones.length > 0 && !$(this.$refs.sliderIntenciones.$el).hasClass('slick-slider')) {
          $(this.$refs.sliderIntenciones.$el).slick(this.sliderConf.sliderT1);
        }

        this.wizardUI.notified = JSON.parse(localStorage.getItem('notified'));
      })
    },
    methods: {
      goToStep: function (step, invalid) {
        if (!invalid) {
          this.wizardUI.stepActive = step;
        }
      },
      obtenerIntenciones: async function (perfil) {
        // this.wizardUI.loading = true;
        $('html, body').animate({
          scrollTop: $('#home-wizard-news').offset().top - 50
        }, 500, 'swing');
        await JRAPP.utils.sleep(550);
        this.goToStep(1, this.$v.formProfileData.$invalid);

        const result = await $.ajax({
          type: "post",
          url: url_ajax,
          data: {
            action: 'listarIntencionesAjax',
            query: {
              'orderby': 'post_date',
              'order': 'ASC',
              'meta_query': [
                {
                  'key': 'perfiles',
                  'value': this.cperfil[0],
                  'compare': 'LIKE'
                }
              ]
            }
          },
          dataType: "json"
        });
        if (this.$refs.sliderIntenciones) $(this.$refs.sliderIntenciones.$el).slick('unslick');
        this.listaIntenciones = result.listaIntenciones;
      },
      cambiarPerfil: function () {
        this.goToStep(0, false);
        this.formProfileData.perfil = false;

      },
      cambiarIntencion: function (params) {
        this.goToStep(2, false);
        this.formIntentData.intencion = false;
      },
      registrarIdea: async function (email) {
        this.wizardUI.loading = true;
        localStorage.setItem('userData', JSON.stringify({ nombre: this.formUserData.nombre, tel: this.formUserData.tel, email: this.formUserData.email }))

        try {
          const notificationForm = JRAPP.modules.popups.$refs.formSuscriberPopup;
          const result = await $.ajax({
            type: "post",
            url: url_ajax,
            data: {
              action: 'registrarIdeaAjax',
              idea: {
                user: this.formUserData,
                perfil: this.cperfil[0],
                intencion: this.cintencion.id,
                descripcion: this.formIdeaData.idea
                //Tengo un negocio de masajes a la que quiero dar una imagen digital. Una web que muestre mis servicio y se pueda realizar reservas.
              }
            },
            dataType: "json"
          });

          this.goToStep(4, this.$v.formIdeaData.$invalid);
          notificationForm.formNotiData.nombre = this.formUserData.nombre;
          notificationForm.formNotiData.email = this.formUserData.email;
          notificationForm.formNotiData.tel = this.formUserData.tel;
          notificationForm.notiUI.inputsbloqued = true;
          notificationForm.$v.formNotiData.nombre.$touch();
          notificationForm.$v.formNotiData.tel.$touch();
          notificationForm.$v.formNotiData.email.$touch();
          await JRAPP.utils.sleep(550);
          this.wizardUI.loading = false;
          // JRAPP.components.page.goTo(this.cintencion.pm, 'intencion');
          if (this.formUserData.notif && !JSON.parse(localStorage.getItem('notified'))) {
            await JRAPP.utils.sleep(5000);
            JRAPP.modules.popups.activePopup = 'notification';
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
  },
  JRAPP.pages.home.DOMReady = function () {
    this.module.instance = new Vue(this.module.options)
    // JRAPP.pages.home.socket();
  };
