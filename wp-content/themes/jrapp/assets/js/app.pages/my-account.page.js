JRAPP.pages["my-account"] = new JRAPP.utils.page("my-account"),
  JRAPP.pages["my-account"].module = { options: null, component: null },
  JRAPP.pages["my-account"].module.options = {
    el: '#my-account',
    name: 'my-account',
    data() {
      return {
        currentUser: null
      }
    },
    created() {
      this.currentUser = JRAPP.modules.header.auth.currentUser;
      if (!this.currentUser) JRAPP.components.page.goTo('/','home');
    }
  },
  JRAPP.pages["my-account"].DOMReady = function () {
    this.module.component = new Vue(this.module.options);
  };
