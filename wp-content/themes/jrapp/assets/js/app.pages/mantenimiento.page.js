JRAPP.pages.mantenimiento = new JRAPP.utils.page("mantenimiento"),
JRAPP.pages.mantenimiento.DOMReady = function () {
    var dpl = $('.mant-countdown').data('deploy'),
        cdd = new Date(dpl).getTime(), //2018-07-05T15:37:25
        cdn = $('.mant-countdown').data('now'),
        dif = 0,
        trs = {};

    var proc = window.setInterval(function () {
        cdn = cdn + 1000;
        dif = cdd - cdn + 1000;
        if (dif > 0) {
            trs = {
                days: Math.floor(dif / (1000 * 60 * 60 * 24)),
                hours: Math.floor((dif % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
                minutes: Math.floor((dif % (1000 * 60 * 60)) / (1000 * 60)),
                seconds: Math.floor((dif % (1000 * 60)) / 1000)
            };

            $('#day strong').text(trs.days >= 10 ? trs.days : '0' + trs.days);
            $('#hour strong').text(trs.hours >= 10 ? trs.hours : '0' + trs.hours);
            $('#minute strong').text(trs.minutes >= 10 ? trs.minutes : '0' + trs.minutes);
            $('#second strong').text(trs.seconds >= 10 ? trs.seconds : '0' + trs.seconds);
        } else {
            clearInterval(proc);
            $('.mant-title').html('<em>¡Estamos listos, actualiza el navegador y </em><strong>Disfruta la aplicación!</strong>');
            $('.mant-countdown').hide();
            $('.mant-update').show();

        }
    }, 1000);
};
