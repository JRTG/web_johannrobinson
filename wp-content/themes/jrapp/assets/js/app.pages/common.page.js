JRAPP.pages.common = new JRAPP.utils.page("common"),
    JRAPP.pages.common.init = function () {
        JRAPP.utils.touch.support(),
            JRAPP.components.menu.init(),
            JRAPP.components.form.init(),
            JRAPP.components.page.init(),
            JRAPP.utils.worker.init();
        setTimeout(function () {
            // JRAPP.components.popup.init();
        }, 3000);
    };
