JRAPP.pages.proyecto = new JRAPP.utils.page("proyecto"),
  JRAPP.pages.proyecto.module = { options: null, instance: null },
  JRAPP.pages.proyecto.module.options = {
    el: '#proyecto',
    name: 'proyecto',
    data() {
      return {
        sliderConf: {
          sliderT2: {
            infinite: false,
            arrows: true,
            dots: true,
            customPaging: function (slider, i) {
              return `<div class="c-slider__dot">${i}</div>`;
            },
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: 1100,
                settings: {
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 1
                }
              }
            ]
          }
        }
      }
    },

    methods: {

    }
  },
  JRAPP.pages.proyecto.DOMReady = function () {
    this.module.instance = new Vue(this.module.options)
  };

