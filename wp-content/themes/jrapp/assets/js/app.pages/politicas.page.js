JRAPP.pages.politicas = new JRAPP.utils.page("politicas"),
  JRAPP.pages.politicas.module = { options: null, component: null },
  JRAPP.pages.politicas.module.options = {
    el: '#politicas',
    name: 'politicas'
  },
  JRAPP.pages.politicas.DOMReady = function () {
    this.module.component = new Vue(this.module.options);
  };
