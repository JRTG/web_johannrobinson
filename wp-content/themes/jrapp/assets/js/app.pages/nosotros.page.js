JRAPP.pages.nosotros = new JRAPP.utils.page("nosotros"),
  JRAPP.pages.nosotros.module = { options: null, component: null },
  JRAPP.pages.nosotros.module.options = {
    el: '#nosotros',
    name: 'nosotros',
    data() {
      return {
        sliderConf: {
          sliderHistoria: {
            infinite: false,
            arrows: false,
            dots: true,
            customPaging: function (slider, i) {
              return `<div class="c-slider__dot">${i}</div>`;
            },
            slidesToShow: 1,
            slidesToScroll: 1
          },
          sliderT2: {
            infinite: false,
            arrows: true,
            dots: true,
            customPaging: function (slider, i) {
              return `<div class="c-slider__dot">${i}</div>`;
            },
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: 1100,
                settings: {
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 1
                }
              }
            ]
          }
        }
      }
    },
    methods: {
      uDecodeHtml(str) {
        let usStr = str.replace(/\\\//g,'/')
        return usStr;
      }
    }
  },
  JRAPP.pages.nosotros.videolist = [],
  JRAPP.pages.nosotros.videos = function () {
    FB.Event.subscribe('xfbml.ready', function (msg) {
      if (msg.type === 'video' && msg.id === 'video-01') {
        // True for <div id="video-01" ...
        JRAPP.pages.nosotros.videolist['video-01'] = msg.instance;
      }
      if (msg.type === 'video' && msg.id === 'video-02') {
        // True for <div id="video-02" ...
        JRAPP.pages.nosotros.videolist['video-02'] = msg.instance;
      }

    });
  },
  JRAPP.pages.nosotros.buildSliders = function () {
    $('.nosotros-historia-slider').slick(this.conf.sliderHistoria);
    $('.c-slider--theme-02').slick(this.conf.sliderT2);
  },
  JRAPP.pages.nosotros.DOMReady = function () {
    this.module.component = new Vue(this.module.options);
    if (typeof FB !== 'undefined') {
      let page = document.getElementById('page');
      FB.XFBML.parse(page);
    }
  };
