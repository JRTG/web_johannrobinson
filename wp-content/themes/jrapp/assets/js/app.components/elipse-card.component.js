Vue.component('c-elipse-card', {
  props: [
    'value',
    'name',
    'data',
    'isInput',
    'isActive'

  ],
  computed: {
    optionValue: function(){
      return `${this.data.id}|${this.data.title}|${this.data.imagen.url}`
    }
  },
  methods: {
    decoder(str) {
      var textArea = document.createElement('textarea');
      textArea.innerHTML = str;
      return textArea.value;
    }
  },
  template: html`
    <label :is="isInput?'label':'div'" :for="isInput?'wi-'+name+'-'+data.id:false" class="c-elipse-card" :class="{'is-active': isInput?value == optionValue:isActive}">
      <div class="c-elipse-card__info">
        <div class="c-elipse-card__img">
          <img :src="data.imagen.url" :alt="data.imagen.alt" width="80" height="80"></img>
        </div>
        <h3 class="c-elipse-card__texto">{{decoder(data.title)}}</h3>
        <div v-if="data.extracto" class="c-elipse-card__desc u-tx-ov-el">{{data.extracto}}</div>
      </div>
      <input v-if="isInput" type="radio" :name="'wi-'+name" :id="'wi-'+name+'-'+data.id" v-bind:value="optionValue" :checked="value == optionValue" v-on:input="$emit('input',$event.target.value)"  v-on:change="$emit('change-option',$event.target.value)">
      <div class="c-elipse-card__controls">
        <div class="g-circle-button g-circle-button--secondary icon-check" v-bind:class="{'is-active': isInput?value == optionValue:isActive}">click</div>
      </div>
    </label>
  `
})
