Vue.component('c-popup', {
  props: [
    'icon',
    'title',
    'show'
  ],
  template: html`
  <div class="g-popup" v-bind:class="show&&'active'">
    <div class="g-popup-cnt">
      <div class="g-popup-inner">
        <span class="g-circle-button g-circle-button--tertiary g-popup-close icon-close"  v-on:click="$emit('close')"></span>
        <div class="g-popup-header">
          <div class="g-popup-icon">
            <img :src="icon" alt="">
          </div>
          <h3 class="g-popup-title">
            <em>{{title[0]}}</em>
            <strong>{{title[1]}}</strong>
          </h3>
        </div>
        <div class="g-popup-scroll-user">
          <div class="g-popup-content">
            <slot></slot>
          </div>
        </div>
      </div>
    </div>
  </div>
  `
})
