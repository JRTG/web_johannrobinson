Vue.component('c-slider', {
  props: {
    theme: {
      type: String,
      default: 'theme-00'
    },
    conf: {
      type: Object
    },
    updated: Object,
    mod:String
  },
  mounted() {
    $(this.$el).slick(this.conf);
  },
  template: html`
    <div class="c-slider" v-bind:class="['c-slider--'+theme,mod]">
      <slot></slot>
    </div>
  `
})
