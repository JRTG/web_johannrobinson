JRAPP.components.youtube = function (pIdCont, pIdVideo, pButton) {
    this.idCont = pIdCont,
        this.idVideo = pIdVideo,
        this.button = pButton
},
    JRAPP.components.youtube.prototype.loadVideo = function () {
        var _this = this;
        if (typeof JRAPP.resources.vPlayers[_this.idCont] !== 'undefined') {
            JRAPP.resources.vPlayers[_this.idCont].destroy();
        }
        JRAPP.resources.vPlayers[_this.idCont] = new YT.Player(_this.idCont, {
            height: '360',
            width: '640',
            videoId: _this.idVideo,
            playerVars: { rel: 0 },
            events: {
                'onStateChange': function (event) {
                    switch (event.data) {
                        case YT.PlayerState.PLAYING:
                        case YT.PlayerState.UNSTARTED:
                            $(_this.button).closest('.gh-item').addClass('play');
                            break;
                        case YT.PlayerState.ENDED:
                        case YT.PlayerState.PAUSED:
                            $(_this.button).closest('.gh-item').removeClass('play');
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        _this.bindEvents();
    },
    JRAPP.components.youtube.prototype.bindEvents = function () {
        var _this = this;
        _this.button.on('click', function (e) {
            e.preventDefault();
            $(this).closest('.gh-item').addClass('play');
            for (var vp in JRAPP.resources.vPlayers) {
                JRAPP.resources.vPlayers[vp].stopVideo();
            }
            JRAPP.resources.vPlayers[_this.idCont].playVideo();
        })
    },
    JRAPP.components.youtube.prototype.playVideo = function () {
        var _this = this;
        JRAPP.resources.vPlayers[_this.idCont].playVideo();
    },
    JRAPP.components.youtube.prototype.stopVideo = function () {
        var _this = this;
        JRAPP.resources.vPlayers[_this.idCont].stopVideo();
    },
    JRAPP.components.youtube.prototype.scann = function () {
        for (var i = 0; i < $('.gh-item').length; i++) {
            var el = $($('.gh-item')[i]),
                pCont = el.find('.g-player'),
                pIdCont = pCont.attr('id'),
                pIdVideo = pCont.data('video'),
                pButton = el.find('.gh-play');

            if (el.find('.g-player.insitu').length > 0) {
                var frame = new JRAPP.components.youtube(pIdCont, pIdVideo, pButton);
                frame.loadVideo();

            }

        }
    },
    JRAPP.components.youtube.prototype.init = function () {
        var _this = this;
        if ($('.YTapi').length > 0) {
            _this.scann();
        } else {
            var tag = document.createElement('script');
            tag.className = 'YTapi';
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            window.onYouTubeIframeAPIReady = function () {
                _this.scann();
            }
        }
    };
