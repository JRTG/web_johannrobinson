JRAPP.components.menu = {
  mobile: {
    states: {
      STATE_DEFAULT: 1,
      STATE_LEFT_SIDE: 2
    },
    currentXPosition: 0,
    rafPending: false,
    initialTouchPos: null,
    lastTouchPos: null,
    currentState: null,
    slopValue: 10
  }
},
  JRAPP.components.menu.contextualizar = function () {
    var fecha = new Date(),
      anio = fecha.getFullYear();

    $('#id_year').text(anio);
    // $('.menu-list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('menu-list');
    // $('.menu-responsive').removeAttr('id');
    // $('.menu-responsive').find('.menu-item').removeClass('menu-item').addClass('menu-item-mobile').removeAttr('id');
    // $('.menu-responsive').find('.menu-list').removeAttr('id');
    // $('.c-header__logo').clone().prependTo('.menu-sidebar-cnt').removeClass('c-header__logo').addClass('menu-logo');
    $('.footer-redes').clone().appendTo('.menu-sidebar-cnt');


    for (var i = 0; i < $('.menu-list .menu-item a').length; i++) {
      var el = $('.menu-list .menu-item a')[i];
      var locUrl = window.location.pathname.replace(/\//gi, '');
      if ($(el).attr('href')) {
        var itHref = $(el).attr('href').split('/')[3];
        $('.menu-list .menu-item a').removeClass('active');
        if (locUrl == itHref) {
          $(el).addClass('active');
          break;
        }
      }
    }
    for (var j = 0; j < $('.menu-responsive .menu-item a').length; j++) {
      var el1 = $('.menu-responsive .menu-item a')[j];
      var locUrl1 = window.location.pathname.replace(/\//gi, '');
      if ($(el1).attr('href')) {
        var itHref1 = $(el1).attr('href').split('/')[3];
        $('.menu-responsive .menu-item a').removeClass('active');
        if (locUrl1 == itHref1) {
          $(el1).addClass('active');
          break;
        }
      }
    }


  },
  JRAPP.components.menu.uiEvents = function () {
    let _this = this;
    $('.menu-mobile-open').click(function (event) {
      _this.mobile.lastTouchPos = { x: event.clientX, y: event.clientY };
      _this.open();
    });
    $('.menu-mobile-close').click(function (event) {
      _this.close();
    });

    $('.menu-overlay').click(function (event) {
      _this.close();
    });

    JRAPP.main.on('click', '.g-ancla', function (event) {
      event.preventDefault();
      var hash = $(this).prop("hash")
      if (hash) {
        $('html, body').animate({
          scrollTop: $(hash).offset().top - 50
        }, 1000, 'swing');
      }
    })

    $(document).keyup(function (event) {
      if (event.which == 27) {
        _this.close();
      }
    });


    _this.mobile.currentState = _this.mobile.states.STATE_DEFAULT;

    const onAnimFrame = function () {
      if (!_this.mobile.rafPending) {
        return;
      }

      var differenceInX = _this.mobile.initialTouchPos.x - _this.mobile.lastTouchPos.x;
      // Limites de movimiento
      if (!((_this.mobile.currentState === _this.mobile.states.STATE_LEFT_SIDE && (differenceInX) > 200) ||
        (_this.mobile.currentState === _this.mobile.states.STATE_DEFAULT && (differenceInX) < 0) ||
        (_this.mobile.currentState === _this.mobile.states.STATE_DEFAULT && (differenceInX) > 200) ||
        (_this.mobile.currentState === _this.mobile.states.STATE_LEFT_SIDE && (differenceInX) > 0) ||
        (_this.mobile.currentState === _this.mobile.states.STATE_LEFT_SIDE && (differenceInX) < -200))) {

        //Movimiento de menu
        var porc = Math.abs(_this.mobile.currentXPosition - differenceInX) / 200;
        var deg = porc * -90;
        let distance = _this.mobile.currentXPosition - differenceInX;

        _this.uiAnimation(porc, deg, distance);
      }
      _this.mobile.rafPending = false;
    }
    JRAPP.utils.touch.setEvent('.menuts', {
      evDown: function (event) {
        event.preventDefault();
        if (!event.target.classList.contains('menuts')) { return false }; // Validamos que no estemos interactuando con hijos de la etiqueta
        if (event.touches && event.touches.length > 1) {
          return;
        }
        // Add the move and end listeners
        if (window.PointerEvent) {
          event.target.setPointerCapture(event.pointerId);
        } else {
          // Add Mouse Listeners
          document.addEventListener('mousemove', this.handleGestureMove, true);
          document.addEventListener('mouseup', this.handleGestureEnd, true);
        }
        _this.mobile.initialTouchPos = JRAPP.utils.touch.getCoords(event);
        $('.menu-sidebar')[0].style.transition = 'initial';
        $('.wrapper')[0].style.transition = 'initial';
        $('.menu-mobile-close')[0].style.transition = 'initial';
        $('.menu-overlay')[0].style.transition = 'initial';
        $('.footer')[0].style.transition = 'initial';
      },
      evMove: function (event) {
        event.preventDefault();
        if (!_this.mobile.initialTouchPos) {
          return;
        }
        _this.mobile.lastTouchPos = JRAPP.utils.touch.getCoords(event);

        if (_this.mobile.rafPending) {
          return;
        }

        _this.mobile.rafPending = true;
        window.requestAnimFrame(onAnimFrame);

      },
      evUp: function (event) {
        event.preventDefault();
        if (!_this.mobile.initialTouchPos) return false;
        if (!event.target.classList.contains('menuts')) return false;
        if (event.touches && event.touches.length > 0) {
          return;
        }
        _this.mobile.rafPending = false;
        // Remove Event Listeners
        if (window.PointerEvent) {
          event.target.releasePointerCapture(event.pointerId);
        } else {
          // Remove Mouse Listeners
          document.removeEventListener('mousemove', this.handleGestureMove, true);
          document.removeEventListener('mouseup', this.handleGestureEnd, true);
        }

        //logic code
        const updateSwipeRestPosition = function () {
          if (!_this.mobile.initialTouchPos) return false;
          var differenceInX = _this.mobile.initialTouchPos.x - _this.mobile.lastTouchPos.x;
          _this.mobile.currentXPosition = _this.mobile.currentXPosition - differenceInX;

          // Go to the default state and change
          var newState = _this.mobile.states.STATE_DEFAULT;

          // Check if we need to change state to left or right based on slporc value
          if (Math.abs(differenceInX) > _this.mobile.slopValue) {
            if (_this.mobile.currentState === _this.mobile.states.STATE_DEFAULT) {
              if (differenceInX > 0) {
                newState = _this.mobile.states.STATE_LEFT_SIDE;
              } else {
                newState = _this.mobile.states.STATE_DEFAULT;
              }
            } else {
              if (_this.mobile.currentState === _this.mobile.states.STATE_LEFT_SIDE && differenceInX > 0) {
                newState = _this.mobile.states.STATE_LEFT_SIDE; // cuando el menu ya se abrió a la IZQ y lo jalamos a la IZQ
              }
            }
          } else {
            newState = _this.mobile.currentState;
          }

          changeState(newState);
        }

        const changeState = function (newState) {
          switch (newState) {
            case _this.mobile.states.STATE_DEFAULT:
              // _this.mobile.currentXPosition = 0;
              _this.close();
              break;
            case _this.mobile.states.STATE_LEFT_SIDE:
              // _this.mobile.currentXPosition = -200;
              _this.open();
              break;
          }
        }
        updateSwipeRestPosition();
        _this.mobile.initialTouchPos = null;
      }
    })


    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      // tasks to do if it is a Mobile Device
      function readDeviceOrientation() {
        if (Math.abs(window.orientation) === 90) {
          // Landscape
          _this.close();
        }
      }
      window.onorientationchange = readDeviceOrientation;
    } else {
      $(window).resize(function () {
        var estadomenu = $('.menu-sidebar-cnt').width();
        if (estadomenu !== 0) {
          _this.close();
        }
      });
    }
  },
  JRAPP.components.menu.uiAnimation = function (percent, degrees, distance, state) {
    let newXTransform = (distance) + 'px';
    let transformStyle = 'translateX(' + newXTransform + ')';

    $('.menu-sidebar')[0].style.webkitTransform = transformStyle;
    $('.menu-sidebar')[0].style.MozTransform = transformStyle;
    $('.menu-sidebar')[0].style.msTransform = transformStyle;
    $('.menu-sidebar')[0].style.transform = transformStyle;

    $('.menu-mobile-close')[0].style.msTransform = transformStyle + ' rotate(' + degrees + 'deg)';
    $('.menu-mobile-close')[0].style.MozTransform = transformStyle + ' rotate(' + degrees + 'deg)';
    $('.menu-mobile-close')[0].style.webkitTransform = transformStyle + ' rotate(' + degrees + 'deg)';
    $('.menu-mobile-close')[0].style.transform = transformStyle + ' rotate(' + degrees + 'deg)';
    $('.menu-mobile-close')[0].style.visibility = percent > 0 ? 'visible' : 'hidden';
    $('.menu-mobile-close')[0].style.opacity = percent;

    $('.wrapper')[0].style.webkitTransform = transformStyle;
    $('.wrapper')[0].style.MozTransform = transformStyle;
    $('.wrapper')[0].style.msTransform = transformStyle;
    $('.wrapper')[0].style.transform = transformStyle;

    $('.footer')[0].style.webkitTransform = transformStyle;
    $('.footer')[0].style.MozTransform = transformStyle;
    $('.footer')[0].style.msTransform = transformStyle;
    $('.footer')[0].style.transform = transformStyle;

    $('.menu-overlay')[0].style.visibility = percent > 0 ? 'visible' : 'hidden';
    $('.menu-overlay')[0].style.opacity = percent > .85 ? .85 : percent;

    if (typeof state !== 'undefined') {
      this.mobile.currentState = state;
      $('.menu-sidebar')[0].style.transition = 'all .2s cubic-bezier(.25,.8,.25,1)';
      $('.wrapper')[0].style.transition = 'all .2s cubic-bezier(.25,.8,.25,1)';
      $('.menu-mobile-close')[0].style.transition = 'all .2s cubic-bezier(.25,.8,.25,1)';
      $('.menu-overlay')[0].style.transition = 'all .2s cubic-bezier(.25,.8,.25,1)';
      $('.footer')[0].style.transition = 'all .2s cubic-bezier(.25,.8,.25,1)';
    }
  },
  JRAPP.components.menu.switch = function (percent, degrees, distance, state, swt) {
    this.mobile.currentXPosition = distance;
    swt = swt ? 'addClass' : 'removeClass';
    $('.menu-sidebar')[swt]('active');
    $('.menu-overlay')[swt]('active');
    $('.menu-mobile-close')[swt]('active');
    $('.menu-mobile-open')[swt]('active');
    $('.wrapper')[swt]('active');
    $('.footer')[swt]('active');
    JRAPP.main[swt]('active');
    this.uiAnimation(percent, degrees, distance, state);
  },
  JRAPP.components.menu.close = function () {
    this.switch(0, 0, 0, this.mobile.states.STATE_DEFAULT, false);
  },
  JRAPP.components.menu.open = function () {
    this.switch(0.85, -90, -200, this.mobile.states.STATE_LEFT_SIDE, true);
  },
  JRAPP.components.menu.init = function () {
    this.contextualizar(),
      this.uiEvents();
  };








