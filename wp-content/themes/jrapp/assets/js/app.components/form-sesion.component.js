Vue.component('c-form-sesion', {
  mixins: [vuelidate.validationMixin, JRAPP.services.user],
  validations: {
    formSesion: {
      email: {
        required: validators.required,
        email: validators.email
      },
      password: {
        required: validators.required
      }
    }
  },
  data() {
    return {
      formSesionUI:{
        loading: false
      },
      formSesion: {
        email: null,
        password: null
      }
    };
  },
  methods: {
    login: function (event) {
      this.formSesionUI.loading = true;
      this.authLogin(this.formSesion.email,this.formSesion.password)
      .then((data) => {
        this.formSesionUI.loading = false;
        localStorage.setItem('currentUser', JSON.stringify(data.user));
        JRAPP.modules.popups.closeAll();
          JRAPP.components.page.goTo('/my-account','my-account');
        })
    }
  },
  mounted() {
  },
  template: html`
  <div class="c-form-suscriber">
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <label for="sesion-email" class="c-input" v-bind:class="{active: $v.formSesion.email.$dirty, error: $v.formSesion.email.$error}">
        <div class="c-label" v-if="$v.formSesion.email.$dirty && !$v.formSesion.email.$error">
          <em>Ingresar email</em>
        </div>
        <div class="c-label error" v-if="$v.formSesion.email.$error">
          <em v-if="!$v.formSesion.email.email">Email no válido</em>
          <em v-if="!$v.formSesion.email.required">Email requerido</em>
        </div>
        <input id="sesion-email" type="text" v-model="formSesion.email" placeholder="Ingresa tu email" v-on:focusout="$v.formSesion.email.$touch()" v-on:input="$v.formSesion.email.$touch()">
      </label>
    </div>
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <label for="sesion-password" class="c-input" v-bind:class="{active: $v.formSesion.password.$dirty, error: $v.formSesion.password.$error}">
        <div class="c-label" v-if="$v.formSesion.password.$dirty && !$v.formSesion.password.$error">
          <em>Contraseña</em>
        </div>
        <div class="c-label error" v-if="$v.formSesion.password.$error">
          <em v-if="!$v.formSesion.password.required">*Contraseña requerida</em>
        </div>
        <input id="sesion-password" type="password" v-model="formSesion.password" placeholder="Ingresa tu contraseña" v-on:focusout="$v.formSesion.password.$touch()" v-on:input="$v.formSesion.password.$touch()">
      </label>
    </div>
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <button v-on:click="login()" :disabled="$v.formSesion.$invalid" class="g-boton" v-bind:class="{'is-loading': formSesionUI.loading}">
        <em>Entrar</em>
      </button>
    </div>
  </div>
  `
})
