Vue.component('c-title', {
  props: {
    controls: Array,
    preTitle:String,
    title: {
      type: Array,
      required: true
    },
    titleTag: {
      type: String,
      default: 'h2'
    },
    icon: Object,
    size: {
      type: String,
      default: 'large'
    },
    mod: String,
    modText: String
  },
  template: html`
    <div class="g-title" v-bind:class="['g-title--'+size,mod,{'has-icon': icon}]">
      <div v-if="icon" class="g-title__icon">
        <img :src="icon.src" :alt="icon.alt">
      </div>
      <div :is="titleTag" class="g-title__text">
        <div v-if="preTitle" class="g-title__text__t3">
          {{preTitle}}
        </div>
        <div class="g-title__text__t1" :class="modText">
          {{title[0]}}
        </div>
        <div class="g-title__text__t2" :class="modText">
          {{title[1]}}
        </div>
      </div>
      <div class="g-title__controls">
        <c-button
          v-for="control in controls"
          v-bind:key="control.id"
          v-bind="control"
        ></c-button>
      </div>
    </div>
  `
})
