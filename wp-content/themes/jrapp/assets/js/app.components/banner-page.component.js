Vue.component('c-banner-page', {
  props: [
    'fondo',
    'title',
    'titlePre',
    'titleTag',
    "titleIcon",
    'titleModText',
    'titleSize',
    'titleControls',
    'image'
  ],
  computed: {
    styleObject: function () {
      let isColor = /(#([\da-f]{3}){1,2}|(rgb|hsl)a\((\d{1,3}%?,\s?){3}(1|0?\.\d+)\)|(rgb|hsl)\(\d{1,3}%?(,\s?\d{1,3}%?){2}\))/ig.test(this.fondo);
      let atributte = isColor ? { backgroundColor: this.fondo } : {backgroundImage: 'url(' + this.fondo + ')'};
      return atributte;
    }
  },
  template: html`
  <section class="g-banner-page" :class="image&&'has-image'">
    <div class="gbp-bg" :style="styleObject"></div>
    <div class="gbp-inner g-area-segura">
      <c-title
        :pre-title="titlePre"
        :title="[title[0],title[1]]"
        :icon = "titleIcon"
        :title-tag="titleTag"
        :size="titleSize"
        :mod-text="titleModText"
        :controls="titleControls"></c-title>
      <div v-if="image" class="gbp-imagen">
        <img :src="image.src" :alt="image.alt">
      </div>
    </div>
  </section>`
})
