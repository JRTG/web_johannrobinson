JRAPP.components.page = {},
  JRAPP.components.page.uiEvents = function () {
    let _this = this;
    JRAPP.main.on('click', '.g-menu-ajax-page a,.g-ajax-page', function (e) {
      e.preventDefault();
      JRAPP.resources.vPlayers = {};
      var rel = $(this).attr('rel'),
        url = $(this).attr('href');

      $('.g-menu-ajax-page a').removeClass('active');
      $('.g-menu-ajax-page a[rel="' + rel + '"]').addClass('active');
      _this.goTo(url, rel);
    });

    window.onpopstate = function (event) {
      var state = event.state;
      if (!state) return false;
      switch (state.type) {
        case 'page':
          JRAPP.resources.vPlayers = {};
          $('.g-menu-ajax-page a').removeClass('active');
          $('.g-menu-ajax-page a[rel="' + state.rel + '"]').addClass('active');

          _this.loadAjax(state.url, state.rel);
          break;
        default:
          break;
      }
    };
  },
  JRAPP.components.page.loadAjax = function (url, rel) {
    JRAPP.modules.header.isLoggedIn();
    JRAPP.modules['menu-mobile'].isLoggedIn();
    $.ajax({
      type: 'GET', url: url,
      beforeSend: function () {
        JRAPP.components.menu.close();
        JRAPP.main.removeClass('loaded begin').addClass('loading end');
        $('meta[name="theme-color"]').attr('content', JRAPP.theme.colorLoading);
      }
    })
      .done(function (data) {
        JRAPP.main.addClass('first');
        setTimeout(() => {
          $(window).scrollTop(0);
          JRAPP.main.removeClass('error end'); //on
          var dom = $(data).contents(),
            pageContent = dom.find('#page').contents();

          let regexTitle = /<title .+title>/i;
          let titleTag = $(regexTitle.exec(data)[0]);
          let title = titleTag.text();

          $('#page').attr('class', '');
          $('#page').addClass(rel);
          $('title').html(title);
          $('#page').html(pageContent);
          if (typeof (ga) !== 'undefined') {
            ga('set', {
              page: window.location.pathname
            });
            ga('send', 'pageview');
          }
          for (var page in JRAPP.pages) {
            if (page == rel) {
              JRAPP.ajaxPage = true;
              JRAPP.pages[page]._DOMReady();
              break;
            }
          }
          setTimeout(function () {
            $('meta[name="theme-color"]').attr('content', JRAPP.theme.colorReady);
            JRAPP.main.removeClass('loading first').addClass('loaded end');
            var observer = lozad();
            observer.observe();
            setTimeout(() => {
              JRAPP.main.addClass('begin');
              setTimeout(() => {
                JRAPP.main.removeClass('end');
                if (window.location.hash) {
                  var hash = window.location.hash;
                  $('html, body').animate({
                    scrollTop: $(hash).offset().top - 50
                  }, 1000, 'swing');
                }
                // setTimeout(() => {
                //   JRAPP.main.addClass('on');
                // }, JRAPP.theme.timeLoad / 2);
              }, JRAPP.theme.timeLoad / 2);

            }, JRAPP.theme.timeLoad / 2);
          }, JRAPP.theme.timeLoad / 2);
        }, JRAPP.theme.timeLoad / 2);

      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        console.log('[Offline] g-loader sin conexión');
        JRAPP.main.addClass('error');
        console.log(jqXHR, textStatus, errorThrown)
      });

    return true;
  },
  JRAPP.components.page.goTo = function (url, rel) {
    if (this.loadAjax(url, rel)) {
      window.history.pushState({ url: url + '?type=page', rel: rel, type: 'page' }, "", url);
    }
  },
  JRAPP.components.page.init = function () {
    this.uiEvents();
  };
