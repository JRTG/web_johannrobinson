Vue.component('c-form-suscriber', {
  mixins: [vuelidate.validationMixin],
  validations: {
    formNotiData: {
      email: {
        required: validators.required,
        email: validators.email
      },
      tel: {
        required: validators.required,
        numeric: validators.numeric,
        minLength: validators.minLength(7),
        maxLength: validators.maxLength(9)
      },
      nombre: {
        required: validators.required,
        alpha: (nombre) => { return /^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñ\s]*$/.test(nombre) }
      },
      poli: {
        checked: function (val) {
          return val
        }
      }
    }
  },
  data() {
    return {
      notiUI: {
        inputsbloqued: (localStorage.getItem('userData')) ? true : false,
      },
      formNotiData: {
        email: localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).email : "",
        tel: localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).tel : "",
        nombre: localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).nombre : "",
        poli: ""
      }
    };
  },
  methods: {
    autorizarNotificaciones: function () {
      const currentuser = {
        email: this.formNotiData.email,
        tel: this.formNotiData.tel,
        nombre: this.formNotiData.nombre
      }
      this.permission(currentuser);
      localStorage.setItem('userData', JSON.stringify({ nombre: this.formNotiData.nombre, tel: this.formNotiData.tel, email: this.formNotiData.email }))
      JRAPP.modules.popups.activePopup = null;
    },
    permission: function (currentuser) {
      return new Promise(function (resolve, reject) {
        const permissionResult = Notification.requestPermission(function (result) {
          resolve(result);
        });

        if (permissionResult) {
          permissionResult.then(resolve, reject);
        }
      })
        .then(function (permissionResult) {
          if (permissionResult == 'granted') {
            // cuando el Usuario Presiona "Permitir" obtenemos el permiso granted
            navigator.serviceWorker.ready.then(serviceWorkerRegistration => serviceWorkerRegistration.pushManager.getSubscription())
              .then(subscription => {
                if (!subscription) {
                  JRAPP.utils.worker.push_subscribe(currentuser);
                }
              })
          } else {
            if (typeof (ga) !== 'undefined') {
              ga('send', {
                hitType: 'event',
                eventCategory: 'Notificación',
                eventAction: 'registrar',
                eventLabel: 'cancelled'
              });
            }
            Cookies.remove('popupNotification');
            throw new Error('No se nos concedió permiso.');
          }
        });
    }

  },
  mounted() {
    if (this.notiUI.inputsbloqued) {
      this.$v.formNotiData.nombre.$touch();
      this.$v.formNotiData.tel.$touch();
      this.$v.formNotiData.email.$touch();
    }
  },
  template: html`
  <div class="c-form-suscriber">
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <label for="notif-nombre" class="c-input" v-bind:class="{active: $v.formNotiData.nombre.$dirty, error: $v.formNotiData.nombre.$error, disabled: notiUI.inputsbloqued}">
        <div class="c-label" v-if="$v.formNotiData.nombre.$dirty && !$v.formNotiData.nombre.$error">
          <em>Ingresar nombre</em>
        </div>
        <div class="c-label error" v-if="$v.formNotiData.nombre.$error">
          <em v-if="!$v.formNotiData.nombre.required">*Nombre requerido</em>
        </div>
        <input id="notif-nombre" type="text" v-model="formNotiData.nombre" :disabled="notiUI.inputsbloqued" placeholder="Dinos tu nombre" v-on:focusout="$v.formNotiData.nombre.$touch()" v-on:input="$v.formNotiData.nombre.$touch()">
      </label>
    </div>
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <label for="notif-tel" class="c-input" v-bind:class="{active: $v.formNotiData.tel.$dirty, error: $v.formNotiData.tel.$error, disabled: notiUI.inputsbloqued}">
        <div class="c-label" v-if="$v.formNotiData.tel.$dirty && !$v.formNotiData.tel.$error">
          <em>Ingresar teléfono</em>
        </div>
        <div class="c-label error" v-if="$v.formNotiData.tel.$error">
          <em v-if="!$v.formNotiData.tel.required">*Teléfono requerido</em>
          <em v-if="!$v.formNotiData.tel.numeric">*Sólo números</em>
          <em v-if="!$v.formNotiData.tel.minLength">*Mínimo 7 números</em>
          <em v-if="!$v.formNotiData.tel.maxLength">*Máximo 9 números</em>
        </div>
        <input id="notif-tel" type="tel" v-model="formNotiData.tel" :disabled="notiUI.inputsbloqued" placeholder="Dinos tu teléfono" v-on:focusout="$v.formNotiData.tel.$touch()" v-on:input="$v.formNotiData.tel.$touch()">
      </label>
    </div>
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <label for="notif-email" class="c-input" v-bind:class="{active: $v.formNotiData.email.$dirty, error: $v.formNotiData.email.$error, disabled: notiUI.inputsbloqued}">
        <div class="c-label" v-if="$v.formNotiData.email.$dirty && !$v.formNotiData.email.$error">
          <em>Ingresar email</em>
        </div>
        <div class="c-label error" v-if="$v.formNotiData.email.$error">
          <em v-if="!$v.formNotiData.email.email">Email no válido</em>
          <em v-if="!$v.formNotiData.email.required">Email requerido</em>
        </div>
        <input id="notif-email" type="text" v-model="formNotiData.email" :disabled="notiUI.inputsbloqued" placeholder="Ingresa tu email" v-on:focusout="$v.formNotiData.email.$touch()" v-on:input="$v.formNotiData.email.$touch()">
      </label>
    </div>
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <label for="notif-poli" class="g-checkbox" v-bind:class="{error: $v.formNotiData.poli.$error}">
        <input id="notif-poli" type="checkbox" v-model="formNotiData.poli" v-on:input="$v.formNotiData.poli.$touch()">
        <span></span>
        <em>Acepto las <a class="c-link g-ajax-page" href="/politicas" rel="politicas">políticas de uso de Datos</a></em>
        <div class="c-label error" v-if="$v.formNotiData.poli.$error">
          <em>Acepta las políticas</em>
        </div>
        <!-- <pre style="font-size: 10px;">{{$v.formNotiData.poli}}</pre> -->
      </label>
    </div>
    <div class="g-field u-flex u-flex-ai-ce u-flex-jc-ce u-flex-fd-sm-c">
      <button v-on:click="autorizarNotificaciones()" :disabled="$v.formNotiData.$invalid" class="g-boton">
        <em>¡Genial!</em>
      </button>
    </div>
  </div>
  `
})
