Vue.component('c-slide', {
  props:{
    mod:String
  },
  template: html`
    <div class="c-slider__item" v-bind:class="[mod]">
      <slot></slot>
    </div>
  `
})
