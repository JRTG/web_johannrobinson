JRAPP.components.form = {},
    JRAPP.components.form.bindEvents = function () {
        $('.c-input input,.c-textarea textarea').off('focusout').on('focusout', function (e) {
            e.preventDefault();
            var valInput = $(this).val().trim();
            if (valInput != '') {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });

        $('.onlyNumbers').off('input').on('input', function (e) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        $('.onlyNoNumbers').off('input').on('input', function (e) {
            this.value = this.value.replace(/[0-9]/g, '');
        });
    },
    JRAPP.components.form.init = function () {
        this.bindEvents();
    };
