Vue.component('c-proyecto-card', {
  props: {
    data: Object
  },
  template: html`
    <div class="c-proyecto-card c-card">
      <a class="c-proyecto-card__protitipo g-ajax-page" :href="data.pm" rel="proyecto">
        <em>Ver más</em>
        <img class="lozad" :data-src="data.prototipo.scrennshot.url" :alt="data.prototipo.scrennshot.alt" width="135">
      </a>
      <a class="c-proyecto-card__top g-ajax-page" :href="data.pm" :style="'background-color:'+data.color" rel="proyecto">
        <div class="c-proyecto-card__cliente-logo">
          <img class="lozad" :data-src="data.empresa.logo.url" :alt="data.empresa.logo.alt" width="75" height="75">
        </div>
        <div class="c-proyecto-card__empresa">
          <div class="c-proyecto-card__categoria">{{data.categoria[0].name}}</div>
          <div class="c-proyecto-card__razonsocial">
            {{data.empresa.nombre}}
          </div>
        </div>
      </a>
      <div class="c-card__controls">
        <c-button :url="data.prototipo.link" theme="circle" icon="icon-reply" target="_blank" rel="noopener" title="Ver más"></c-button>
      </div>
      <a class="c-proyecto-card__bottom g-ajax-page" :href="data.pm" rel="proyecto">
        <div class="c-proyecto-card__desc u-tx-ov-el">
          {{data.extracto}}
        </div>
      </a>
    </div>
  `
})
