Vue.component('c-post-card', {
  props: {
    data: Object
  },
  template: html`
    <div class="c-post-card c-card">
      <a class="c-post-card__top g-ajax-page" :href="data.pm" rel="post">
        <div class="c-post-card__img">
          <img class="lozad" :data-src="data.banner.sizes.medium_large" :alt="data.banner.alt">
        </div>
        <div class="c-post-card__fecha">{{data.date}}</div>
      </a>
      <div class="c-card__controls">
        <c-button :url="data.pm" theme="circle" icon="icon-share" todo="share" title="Compartir" v-on:click-button=""></c-button>
      </div>
      <a class="c-post-card__bottom g-ajax-page" :href="data.pm" rel="post">
        <div class="c-post-card__title u-tx-ov-el u-ws-nw">{{data.title}}</div>
        <div class="c-post-card__categoria">{{data.categoria[0]['cat_name']}}</div>
        <div class="c-post-card__desc u-tx-ov-el">
          {{data.extracto}}
        </div>
      </a>
    </div>
  `
})
