Vue.component('c-button', {
  props: {
    url: String,
    todo: String,
    icon: String,
    mod2: String,
    mod: {
      type: String,
      default: 'primary'
    },
    tag: {
      type: String,
      default: 'a'
    },
    theme: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    target: {
      type: [String, Boolean]
    },
    rel: {
      type: [String, Boolean]
    }
  },
  computed: {
    eClick: function () {
      switch (this.todo) {
        case 'share':
          eClick = 'shareUrl';
          break;
        case 'ancla':
          eClick = 'goToAnchor';
          break;
        case 'popup':
          eClick = 'openPopup';
          break;
        case 'internal':
          eClick = 'navigateTo';
          break;
      }
      return eClick;
    }
  },
  methods: {
    shareUrl() {
      JRAPP.utils.facebook.share(this.url);
    },
    openPopup() {
      var hash = this.url;
      if (hash) {
        hash = hash.split('#')[1];
        JRAPP.modules.popups.activePopup = hash;
      }
    },
    goToAnchor() {
      var hash = this.url;
      if (hash) {
        hash = hash.split('#')[1];
        $('html, body').animate({
          scrollTop: $(`#${hash}`).offset().top - 50
        }, 1000, 'swing');
      }
    },
    navigateTo() {
      JRAPP.resources.vPlayers = {};
      $('.g-menu-ajax-page a').removeClass('active');
      $('.g-menu-ajax-page a[rel="' + this.rel + '"]').addClass('active');
      // JRAPP.modules.header.activeMenu = rel;
      JRAPP.components.page.goTo(this.url, this.rel);
    },
    runMethod(method,event) {
      event.preventDefault();
      console.log(event);
      this[method]();
    }
  },
  template: html`
  <a
    :is="tag"
    class="g-boton"
    v-if="theme == 'regular'"
    v-bind:href="url"
    v-on:click="todo?(todo=='external'?null:runMethod(eClick,$event)):$emit('click-button')"
    v-bind:rel="rel?rel:false"
    v-bind:target="target?target:false" >
    <em>{{title}}</em>
  </a>

  <a
    :is="tag"
    class="g-circle-button"
    v-else="theme == 'circle'"
    v-bind:href="url"
    v-bind:class="['g-circle-button--'+mod, mod2, icon]"
    v-on:click="todo?(todo=='external'?null:runMethod(eClick,$event)):$emit('click-button')"
    v-bind:rel="rel?rel:false"
    v-bind:target="target?target:false">
    <em>{{title}}</em>
  </a>
  `
})
