self.addEventListener('push', function (event) {
  if (!(self.Notification && self.Notification.permission === 'granted')) {
    return;
  }

  const sendNotification = body => {
    // you could refresh a notification badge here with postMessage API
    body = JSON.parse(body);
    return self.registration.showNotification(body.title, body.options);
  };

  if (event.data) {
    const message = event.data.text();
    event.waitUntil(sendNotification(message));
  }
});

self.addEventListener('notificationclick', function (event) {
  console.log('[Service Worker] Notification click Received.');
  event.notification.close();
  if (event.notification.data !== null && event.notification.data.url) {
    event.waitUntil(
      clients.openWindow(event.notification.data.url)
    );
  }
});

self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open('jrapp-files').then(function (cache) {
      return cache.addAll(
        [
          '/wp-content/themes/jrapp/assets/css/styles.css',
          '/wp-content/themes/jrapp/assets/css/styles-small.css',
          // '/wp-content/themes/jrapp/assets/media/brand/logo.svg',
          '/wp-content/themes/jrapp/assets/media/brand/256x256.png',
          '/wp-content/themes/jrapp/assets/media/brand/114x114.png',
          '/wp-content/themes/jrapp/assets/media/brand/72x72.png',
          '/wp-content/themes/jrapp/assets/media/brand/57x57.png',
          '/wp-content/themes/jrapp/assets/css/styles-admin.css',



          '/wp-content/themes/jrapp/assets/fonts/icomoon.eot?t7rw0d',
          '/wp-content/themes/jrapp/assets/fonts/icomoon.ttf?t7rw0d',
          '/wp-content/themes/jrapp/assets/fonts/icomoon.woff?t7rw0d',
          '/wp-content/themes/jrapp/assets/fonts/icomoon.svg?t7rw0d',

          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Regular.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Regular.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Regular.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Regular.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-SemiBold.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-SemiBold.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-SemiBold.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-SemiBold.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Thin.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Thin.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Thin.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Thin.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Light.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Light.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Light.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Light.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Medium.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Medium.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Medium.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Medium.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraLight.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraLight.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraLight.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraLight.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraBold.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraBold.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraBold.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-ExtraBold.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Bold.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Bold.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Bold.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Bold.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Black.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Black.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Black.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Raleway-Black.svg',

          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Black.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Black.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Black.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Black.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Bold.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Bold.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Bold.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Bold.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-ExtraBold.eot',

          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-ExtraBold.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-ExtraBold.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-ExtraBold.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Heavy.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Heavy.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Heavy.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Heavy.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Medium.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Medium.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Medium.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Medium.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Regular.eot',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Regular.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Regular.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Regular.svg',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Light.eot',

          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Light.woff',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Light.ttf',
          // '/wp-content/themes/jrapp/assets/fonts/Gilroy-Light.svg',

          // 'wp-content/themes/jrapp/assets/js/app.libraries/jquery/jquery-2.0.min.js',
          // 'wp-content/themes/jrapp/assets/js/app.libraries/slick/slick-theme.css',
          // 'wp-content/themes/jrapp/assets/js/app.libraries/slick/slick.css',
          // 'wp-content/themes/jrapp/assets/js/app.libraries/slick/slick.min.js',
          // 'wp-content/themes/jrapp/assets/js/app.libraries/deltasc/deltasc.min.js',
          // '/wp-content/themes/jrapp/assets/js/app.main.js'
        ]
      );
    })
  );
});

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.filter(function (cacheName) {
          // Return true if you want to remove this cache,
          // but remember that caches are shared across
          // the whole origin
        }).map(function (cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});


self.addEventListener('fetch', function (event) {
  let urlsRestrict = /wp-admin/.test(event.request.url) || /wp-login.php/.test(event.request.url) || /wp-json/.test(event.request.url);
  if (urlsRestrict) return true;

  event.respondWith(
    caches.open('jrapp-dynamic')
      .then(function (cache) {
        return cache.match(event.request)
          .then(function (response) {
            var fetchPromise = fetch(event.request)
              .then(function (networkResponse) {
                cache.put(event.request, networkResponse.clone());
                return networkResponse;
              })
            return response || fetchPromise;
          })
      })
  );
})
