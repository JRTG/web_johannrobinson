JRAPP.utils.facebook = {},
  JRAPP.utils.facebook.share = function (url) {
    var url = (typeof url !== 'undefined' && url !== '') ? url : window.location.href;
    if (window.mobilecheck()) {
      navigator.share({
        title: document.title,
        text: 'Hola, te invito a compartir esta app',
        url: url
      });
    } else {
      //Habilitar el share de facebook 861111000951187
      FB.ui({
        display: 'popup',
        method: 'share',
        href: url,
      }, function (response) { });
    }
  },
  JRAPP.utils.facebook.initApp = function () {
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/es_LA/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function () {
      FB.init({
        appId: '146569586036682',
        cookie: true,
        status: true,
        xfbml: true,
        version: 'v4.0'
      });

      FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          console.log('1Carga:El usuario esta en el app de Facebook');
          transition('1');
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          console.log('1Carga:El usuario esta logueado en Facebook, pero no en el app');
          transition('1');

        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          console.log('1Carga:El usuario no se ha logueado en facebook, evidentemente');
          transition('1');

        }
      });
      JRAPP.pages.nosotros.videos();

    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk-msn'));
  },
  JRAPP.utils.facebook.iniciarSesion = async function () {
    // FB.login(function (response) {
    //   if (response.status === 'connected') {
    //     // Logged into your app and Facebook.
    //     // console.log('Login:El usuario esta logueado en el app de Facebook');
    //     FB.api('/me', { fields: 'name,email,picture' }, function (response) {
    //       console.log(response);
    //     });
    //   } else if (response.status === 'not_authorized') {
    //     // The person is logged into Facebook, but not your app.
    //     // console.log('Login:El usuario esta logueado, pero no en el app');
    //   } else {
    //     // The person is not logged into Facebook, so we're not sure if
    //     // they are logged into this app or not.
    //     // console.log('Login:El usuario no se ha logueado en facebook, evidentemente');
    //     // transition('wiz1');
    //   }
    // }, { scope: 'public_profile,email' });
  };
