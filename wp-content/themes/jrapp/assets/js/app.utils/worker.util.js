JRAPP.utils.worker = {
  applicationServerKey: 'BEYMDSgBw9MfCb2NR420yoEQc1Srro1ftGWRfXpO3q5ge164bT9jHeCkb2gxg139tfKUkwRextgVBoUlHk-HKYM'
},
  JRAPP.utils.worker.registrar = function () {
    console.log('comprobando');
    let _this = this;
    if (!('serviceWorker' in navigator)) {
      console.warn("Service workers are not supported by this browser");
      return;
    }

    if (!('PushManager' in window)) {
      console.warn('Push notifications are not supported by this browser');
      return;
    }

    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
      console.warn('Notifications are not supported by this browser');
      return;
    }

    // Check the current Notification permission.
    // If its denied, the button should appears as such, until the user changes the permission manually
    if (Notification.permission === 'denied') {
      console.warn('Notifications are denied by the user');
      return;
    }

    return navigator.serviceWorker.register('/app.worker.js')
      .then(function (reg) {
        // prK = 'M5C5rFXe9gbOvJk4R15w1bYZkPqxYdov3U8YU-kYUek';
        // return registration.pushManager.subscribe(subscribeOptions);
        reg.installing; // the installing worker, or undefined
        reg.waiting; // the waiting worker, or undefined
        reg.active; // the active worker, or undefined
        reg.addEventListener('updatefound', () => {
          // A wild service worker has appeared in reg.installing!
          const newWorker = reg.installing;

          newWorker.state;
          // "installing" - the install event has fired, but not yet complete
          // "installed"  - install complete
          // "activating" - the activate event has fired, but not yet complete
          // "activated"  - fully active
          // "redundant"  - discarded. Either failed install, or it's been
          //                replaced by a newer version

          newWorker.addEventListener('statechange', () => {
            // newWorker.state has changed
          });
        });

        _this.push_updateSubscription();
      }, function (e) {
        console.error('[SW] Service worker registration failed', e);
      });
  },
  JRAPP.utils.worker.push_updateSubscription = function () {
    let _this = this;
    navigator.serviceWorker.ready.then(serviceWorkerRegistration => serviceWorkerRegistration.pushManager.getSubscription())
      .then(subscription => {

        if (!subscription) {
          console.log('[No suscritos] No estamos suscritos a push, por lo tanto, configure la interfaz de usuario para permitir que el usuario habilite push');

          localStorage.setItem('notified', false);
          JRAPP.modules.header.buttons.notification = true;
          var nroVisit = parseInt(Cookies.get('pVisitUser'));

          if (nroVisit >= 3) { //Evaluamos si el usuario tiene más de tres visitas
            if (typeof Cookies.get('popupNotification') == "undefined") { // Evaluamos si ya le mostramos el popupNotification
              if (!$('#page').hasClass('mantenimiento')) {
                Cookies.set('popupNotification', 1, { expires: 1 }); // En caso de no haberle mostrado el popupNotification seteamos una cookie de 1 día de duración
                JRAPP.modules.popups.activePopup = 'notification';
              }
              // if (Notification.permission !== 'granted') {
              // }
            }
          }
          return;
        }

        // Mantenga su servidor sincronizado con el último punto final
        return _this.push_sendSubscriptionToServer(subscription, 'PUT');
      })
      .then(subscription => {
        if (subscription) {
          JRAPP.modules.header.buttons.notification = false;
        }
        // subscription && console.log('ya estás suscripto');

      }) // Configure su UI para mostrar que se suscribió a mensajes push
      .catch(e => {
        console.error('[Error al actualizar] la suscripción', e);
      });
  },
  JRAPP.utils.worker.push_subscribe = function (currentuser) {
    let _this = this;
    navigator.serviceWorker.ready
      .then(serviceWorkerRegistration => serviceWorkerRegistration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: _this.urlBase64ToUint8Array(_this.applicationServerKey),
      }))
      .then(subscription => {
        // La suscripción fue exitosa
        // crea una suscripción en tu servidor
        return _this.push_sendSubscriptionToServer(subscription, 'POST', currentuser);
      })
      .catch(e => {
        if (Notification.permission === 'denied') {
          // El usuario denegó el permiso de notificación que
          // significa que no nos hemos podido suscribir y el usuario necesitará
          // para cambiar manualmente el permiso de notificación a
          // suscribirse a mensajes push
          console.warn('Notifications are denied by the user.');
        } else {
          // Ha ocurrido un problema con la suscripción; razones comunes
          // incluye errores de red o el usuario se saltó el permiso
          console.error('Impossible to subscribe to push notifications', e);
        }
      });
  },
  JRAPP.utils.worker.push_unsubscribe = function () {
    console.log('computing');
    let _this = this;
    // To unsubscribe from push messaging, you need to get the subscription object
    navigator.serviceWorker.ready
      .then(serviceWorkerRegistration => serviceWorkerRegistration.pushManager.getSubscription())
      .then(subscription => {
        // Check that we have a subscription to unsubscribe
        if (!subscription) {
          // No subscription object, so set the state
          // to allow the user to subscribe to push
          console.log('No hay una suscripción definida');
          return;
        }

        // We have a subscription, unsubscribe
        // Remove push subscription from server
        Cookies.remove('popupNotification');
        subscription.unsubscribe();
        return _this.push_sendSubscriptionToServer(subscription, 'DELETE');
      })
      // .then(subscription => subscription.unsubscribe())
      // .then(() => {console.log('disabled')})
      .catch(e => {
        // We failed to unsubscribe, this can lead to
        // an unusual state, so  it may be best to remove
        // the users data from your data store and
        // inform the user that you have done so
        console.error('Error when unsubscribing the user', e);
      });
  },
  JRAPP.utils.worker.push_sendSubscriptionToServer = function (subscription, pmethod, currentuser) {
    const key = subscription.getKey('p256dh');
    const token = subscription.getKey('auth');
    const contentEncoding = (PushManager.supportedContentEncodings || ['aesgcm'])[0];
    return $.ajax({
      type: 'POST',
      url: url_ajax,
      data: {
        action: 'registrarSuscripcion',
        method: pmethod,
        nombre: typeof currentuser != 'undefined' ? currentuser.nombre : null,
        email: typeof currentuser != 'undefined' ? currentuser.email : null,
        tel: typeof currentuser != 'undefined' ? currentuser.tel : null,
        endpoint: subscription.endpoint,
        publicKey: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
        authToken: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
        contentEncoding
      },
      dataType: "json",
      success: function (response) {
        switch (response.estado) {
          case 1:
            // if (JRAPP.pages.home.module.component) {
            //     JRAPP.pages.home.module.component.wizardUI.notified = true;
            // }
            localStorage.setItem('notified', true);
            JRAPP.modules.header.buttons.notification = false;

            break;
          case 3:
            // actualizada
            // if (JRAPP.pages.home.module.component) {
            //     JRAPP.pages.home.module.component.wizardUI.notified = true;
            // }
            localStorage.setItem('notified', true);
            break;
          case 4:
            JRAPP.modules.header.buttons.notification = true;
            // if (JRAPP.pages.home.module.component) {
            //     JRAPP.pages.home.module.component.wizardUI.notified = false;
            // }
            localStorage.setItem('notified', false);
            localStorage.removeItem('userData')
            break;

          default:
            break;
        }
      }
    }).then(() => subscription);
  },
  JRAPP.utils.worker.urlBase64ToUint8Array = function (base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  },
  JRAPP.utils.worker.installApp = function () {

    window.addEventListener('beforeinstallprompt', function (e) {
      e.preventDefault();
      console.log('[Mostrar Popup de instalación]');

      //[Mostrar boton de instalación de App en header]
      JRAPP.modules.header.buttons.install = true;

      // Para recomendar la instalación Contaremos 3 visitas en un 2 días y la guardaremos en una cookie.
      var nroVisit = parseInt(Cookies.get('pVisitUser'));
      if (nroVisit >= 3) {
        //Colocamos un observador para detectar la navegación en scroll del usuario y activar el PopupSetup
        var options = {
          rootMargin: '0px',
          threshold: 0
        }
        var callback = function (entries) {
          entries.forEach(function (entry) {
            if (entry.isIntersecting) {
              if (typeof Cookies.get('popupSetup') == "undefined") {
                Cookies.set('popupSetup', 1, { expires: 1 });
                JRAPP.modules.popups.activePopup = 'setup'
              }
            }

          });

        };

        var observerLaunchSetup = new IntersectionObserver(callback, options);

        // "launchSetup" está ubicado en el footer
        var targetSetup = $('.launchSetup')[0];
        observerLaunchSetup.observe(targetSetup);

      }
      // Guarda el evento para que pueda activarse más tarde.
      deferredPrompt = e;

      return false;
    });

    $('#dt-setup').on('click', function (e) {
      e.preventDefault();
      if (deferredPrompt !== undefined) {
        // The user has had a postive interaction with our app and Chrome
        // has tried to prompt previously, so let's show the prompt.
        deferredPrompt.prompt();

        // Follow what the user has done with the prompt.
        deferredPrompt.userChoice.then(function (choiceResult) {

          console.log(choiceResult.outcome);

          if (choiceResult.outcome == 'dismissed') {
            console.log('User cancelled home screen install');
          }
          else {
            console.log('User added to home screen');
          }

          // We no longer need the prompt.  Clear it up.
          deferredPrompt = null;
          JRAPP.modules.popups.activePopup = null;
          JRAPP.modules.header.buttons.install = false;
        });
      }
    });
  },
  JRAPP.utils.worker.contarVisitas = function () {
    if (typeof Cookies.get('pVisitUser') == "undefined") {
      Cookies.set('pVisitUser', 0, { expires: 1 });
    }
    var counterVisit = parseInt(Cookies.get('pVisitUser')) + 1;
    Cookies.set('pVisitUser', counterVisit, { expires: 1 });
  },
  JRAPP.utils.worker.init = function () {
    this.contarVisitas();
    this.registrar();
    this.installApp();
  };
