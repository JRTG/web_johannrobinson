JRAPP.utils.touch = {},
    JRAPP.utils.touch.support = function () {
        window.requestAnimFrame = (function () {
            'use strict';

            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        /* // [START pointereventsupport] */
        var pointerDownName = 'pointerdown';
        var pointerUpName = 'pointerup';
        var pointerMoveName = 'pointermove';

        if (window.navigator.msPointerEnabled) {
            pointerDownName = 'MSPointerDown';
            pointerUpName = 'MSPointerUp';
            pointerMoveName = 'MSPointerMove';
        }

        // Simple way to check if some form of pointerevents is enabled or not
        window.PointerEventsSupport = false;
        if (window.PointerEvent || window.navigator.msPointerEnabled) {
            window.PointerEventsSupport = true;
        }
        /* // [END pointereventsupport] */
    },
    JRAPP.utils.touch.setEvent = function (elements, callback) {
        elements = $(elements);
        for (let i = 0; i < elements.length; i++) {
            const element = elements[i];
            /* // [START addlisteners] */
            // Check if pointer events are supported.
            if (window.PointerEvent) {
                // Add Pointer Event Listener
                element.addEventListener('pointerdown', callback.evDown, true);
                element.addEventListener('pointermove', callback.evMove, true);
                element.addEventListener('pointerup', callback.evUp, true);
                element.addEventListener('pointercancel', callback.evUp, true);
            } else {
                // Add Touch Listener
                element.addEventListener('touchstart', callback.evDown, true);
                element.addEventListener('touchmove', callback.evMove, true);
                element.addEventListener('touchend', callback.evUp, true);
                element.addEventListener('touchcancel', callback.evUp, true);

                // Add Mouse Listener
                element.addEventListener('mousedown', callback.evDown, true);
            }
            /* // [END addlisteners] */
        }
        
        if (/iP(hone|ad)/.test(window.navigator.userAgent)) {            
            var emptyFunction = function () { };
            for (var i = 0; i < elements.length; i++) {
                elements[i].addEventListener('touchstart', emptyFunction, false);
            }
        }
    },
    JRAPP.utils.touch.getCoords = function (e) {
        var point = {};

        if (e.targetTouches) {
            point.x = e.targetTouches[0].clientX;
            point.y = e.targetTouches[0].clientY;
        } else {
            // Either Mouse event or Pointer Event
            point.x = e.clientX;
            point.y = e.clientY;
        }

        return point;
    }