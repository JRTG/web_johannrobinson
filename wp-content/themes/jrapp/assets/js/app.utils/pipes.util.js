Vue.mixin({
  methods: {
    uJsonParse(str) {
      let nolnStr = str.replace(/\r?\n|\r/g,'').replace(/<!--(.*?)-->/g,'');
      let newStr = nolnStr.replace(/<.+?>/g,function(matchStr){
        let replacement = matchStr.replace(/\//g,'\\/').replace(/"/g,'\\"');
        return replacement;
      })
      return JSON.parse(newStr);
    },
    debounce(func, wait, immediate) {
      var timeout;
      return function () {
        var context = this, args = arguments;
        var later = function () {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  }
})
