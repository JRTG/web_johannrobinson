(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() : (global.lozad = factory());
  // typeof define === 'function' && define.amd ? define(factory) :

}(this, (function () {
  'use strict';

  var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

  /**
   * Detect IE browser
   * @const {boolean}
   * @private
   */
  var isIE = document.documentMode;

  var defaultConfig = {
    rootMargin: '0px',
    threshold: 0,
    load: function load(element) {
      if (element.nodeName.toLowerCase() === 'picture') {
        var img = document.createElement('img');
        if (isIE && element.getAttribute('data-iesrc')) {
          img.src = element.getAttribute('data-iesrc');
        }
        element.appendChild(img);
      }
      if (element.getAttribute('data-src')) {
        element.src = element.getAttribute('data-src');
      }
      if (screen.width > 768) {
        if (element.getAttribute('data-srcd')) {
          element.src = element.getAttribute('data-srcd');
        }
      } else if (screen.width > 480 && screen.width < 768) {
        if (element.getAttribute('data-srct')) {
          element.src = element.getAttribute('data-srct');
        }
      } else if (screen.width < 480) {
        if (element.getAttribute('data-srcm')) {
          element.src = element.getAttribute('data-srcm');
        }
      }
      if (element.getAttribute('data-srcset')) {
        element.srcset = element.getAttribute('data-srcset');
      }
      if (element.getAttribute('data-background-image')) {
        element.style.backgroundImage = 'url(' + element.getAttribute('data-background-image') + ')';
      }
      imgResponsiveLazy(element)
    },
    loaded: function loaded() {

    }
  };

  function imgResponsiveLazy(element) {
    var WIDTH_ACTUAL = window.innerWidth;
    var dtsrcset = element.getAttribute('data-srcset'),
      dtsize = element.getAttribute('data-size'),
      dtbgset = element.getAttribute('data-bgset');
    if (dtsize < WIDTH_ACTUAL) {
      if (dtsrcset) {
        element.src = dtsrcset.split(',')[0];
      }
      if (dtbgset) {
        element.style.backgroundImage = 'url(' + dtbgset.split(',')[0] + ')';
      }
    } else if (dtsize >= WIDTH_ACTUAL) {
      if (dtsrcset) {
        element.src = dtsrcset.split(',')[1];
      }
      if (dtbgset) {
        element.style.backgroundImage = 'url(' + dtbgset.split(',')[1] + ')';
      }
    }
  }

  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  function windowResize(elements) {
    var myEfficientFn = debounce(function () {
      for (var i = 0; i < elements.length; i++) {
        var element = elements[i];

        if (screen.width > 768) {
          if (element.getAttribute('data-srcd')) {
            element.src = element.getAttribute('data-srcd');
          }
        } else if (screen.width > 480 && screen.width < 768) {
          if (element.getAttribute('data-srct')) {
            element.src = element.getAttribute('data-srct');
          }
        } else if (screen.width < 480) {
          if (element.getAttribute('data-srcm')) {
            element.src = element.getAttribute('data-srcm');
          }
        }
        imgResponsiveLazy(element)
      }
    }, 250);

    window.addEventListener("resize", myEfficientFn);
  }

  function markAsLoaded(element) {
    element.setAttribute('data-loaded', true);
    element.parentNode.classList.add("load")
    // element.parentElement.style.minHeight = 'initial';
  }

  var isLoaded = function isLoaded(element) {
    return element.getAttribute('data-loaded') === 'true';
  };

  var onIntersection = function onIntersection(load, loaded) {
    return function (entries, observer) {
      entries.forEach(function (entry) {
        if (entry.intersectionRatio > 0) {
          observer.unobserve(entry.target);

          if (!isLoaded(entry.target)) {
            load(entry.target);
            markAsLoaded(entry.target);
            loaded(entry.target);
          }

        }
      });
    };
  };

  var getElements = function getElements(selector) {
    if (selector instanceof Element) {
      return [selector];
    }
    if (selector instanceof NodeList) {
      return selector;
    }
    return document.querySelectorAll(selector);
  };

  var lozad = function () {
    var selector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '.lozad';
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var _defaultConfig$option = _extends({}, defaultConfig, options),
      rootMargin = _defaultConfig$option.rootMargin,
      threshold = _defaultConfig$option.threshold,
      load = _defaultConfig$option.load,
      loaded = _defaultConfig$option.loaded;

    var observer = void 0;

    if (window.IntersectionObserver) {
      observer = new IntersectionObserver(onIntersection(load, loaded), {
        rootMargin: rootMargin,
        threshold: threshold
      });
    }

    return {
      observe: function observe() {
        var elements = getElements(selector);

        for (var i = 0; i < elements.length; i++) {
          if (isLoaded(elements[i])) {
            continue;
          }
          if (observer) {
            observer.observe(elements[i]);
            continue;
          }
          load(elements[i]);
          markAsLoaded(elements[i]);
          loaded(elements[i]);
        }

        windowResize(elements);
      },
      triggerLoad: function triggerLoad(element) {
        if (isLoaded(element)) {
          return;
        }

        load(element);
        markAsLoaded(element);
        loaded(element);
      }
    };
  };

  return lozad;

})));
