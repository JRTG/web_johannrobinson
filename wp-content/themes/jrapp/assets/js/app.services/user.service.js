JRAPP.services.user = {
  methods: {
    authLogin: function (user, pass) {
      return new Promise((resolve,rejected)=>{
        try {
          const result = $.ajax({
            type: "post",
            url: url_ajax,
            data: {
              action: 'login',
              username: user,
              password: pass
            },
            dataType: "json"
          });

          resolve(result);
        } catch (error) {
          rejected(error)
        }
      })
    },
    authLogout: function () {
      return new Promise((resolve,rejected)=>{
        try {
          const result = $.ajax({
            type: "post",
            url: url_ajax,
            data: {
              action: 'logout'
            },
            dataType: "json"
          });

          resolve(result);
        } catch (error) {
          rejected(error)
        }
      })
    }
  }
}
